# OFSCA (Oriented Fractal Scaling Component Analysis)

## Article describing theory and practical application of OFSCA  

Seleznov, I., Popov, A., Kikuchi, K. et al. Detection of oriented fractal scaling components in anisotropic two-dimensional trajectories. 
Sci Rep 10, 21892 (2020). https://doi.org/10.1038/s41598-020-78807-z

## Contribution

Feel free to add suggestions, PRs, comments and bug reports.

## Installation

### pip

`pip install git+https://gitlab.com/IvanSeleznov/ofsca.git`

### From source 

#### Linux

1. Download the package and extract it into a local directory.

2. Open a command window and cd into the directory, and run: 

`python setup.py install`

#### Windows

1. Download the package and extract it into a local directory. 

2. Download and Install Git
 
3. Open MinGW console and go into the package directory.

4. Run: 

`python setup.py install`



## How to perform OFSCA (General Procedure) 

1. We first start we creating OFSCA object in python. 
    
    ```
   from ofsca import ofsca
   
    ofsca_obj = ofsca.OFSCA(x_vector=data.cop_x,
                            y_vector=data.cop_y,
                            order=4,
                            start_scale=1.2,
                            end_scale=2.5,
                            make_integration=True,
                            n_angles=64)
    ```                        
    
    There are several mandatory parameters, that needed to be passed to the OFSCA class: 

     - x_vector: [1-D numpy array] X axis of 2-D trajectory
     - y_vector: [1-D numpy array] Y axis of 2-D trajectory
     - order - [0, 2, 4] order of detrending. describes what polynomial degree trend is removed during the detrending procedure 
     - start_scale - [int/float] start values of scale for calculating local slopes on the log-log plots in each direction to obtain orientations of the main components
     - end_scale - [int/float] end values of scale for calculating local slopes on the log-log plots in each direction to obtain orientations of the main components
     - make_integration - [True/False] pass True if integration of the raw time series (x_vector/y_vector) is needed prior to analysis
     - n_angles - [int] number of angles to calculate DDMA for, (180/n_angles) = angle_step
        
    There is another parameter `refresh` that is described below.
    
2. Afterwards the following method should be called to perform the OFSCA calculation
    
    ```
   ofsca_obj.perform_OFSCA()
   ```
3. To plot the common results figure, presented in the article, call the following function: 

    ```
   fig = ofsca_obj.get_ofsca_fig(rcParams={'font.size': 14}, alpha_graphs_ylim=[0.4, 2.8])
   ```
   There are several parameters: 
   
    - rcParams: a typical matplotlib parameters dictionary, refer here
    - alpha_graphs_ylim: [list], [min, max] list of min/max values to normalize scaling exponent (Hurst exponent) plots. If not defined, then local min and max value will be used.
   
4. Then use the matplotlib.pyplot plot() function to plot the resulting figure.

5. If you're interested in the main components parameters (ex. main directions, alpha exponent, original timeseries, etc.), please extract them from [this class](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/ofsca.py#L135). 
    
    - ofsca_obj.comp1.theta/ofsca_obj.comp2.theta - orientations of the main components [in radians];
    - ofsca_obj.comp1.alpha/ofsca_obj.comp2.alpha - scaling exponents (Hurst exponents) of the main components;
    - ofsca_obj.comp1.ts/ofsca_obj.comp2.ts - time series of the main components;
    - ofsca_obj.comp1.[log_s/log_F]/ofsca_obj.comp2.[log_s/log_F] - Directed DMA log fluctuation function and log scale results for main components;

6. If you want to calculate the slope (Hurst exponent) of the main components log-log plots in the custom scale range, not defined by start_scale/end_scale parameter, please use [this function of calculating local slope](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/ofsca.py#L522), included in the package. 
7. If you need more advanced analysis (splitting time series, normalizing surface plots, etc.), please, take a look at the examples source code below.
8. If you need to rearrange or modify the results figure, please, take a look at the [main method for visualizing](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/ofsca.py#L626) to get the idea how to rearrange or modify polar plots and the resulting figures. 

## Main algorithm  

   If you open the contents of the ```ofsca_obj.perform_OFSCA()``` method you can understand the main procedure of the OFSCA analysis: 
   
   * At first we calculate the [DDMA (Directed Detrended Moving Average)](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/ddma/ddma.c), which is developed as a C extension package. 
   ```
   # get DDMA analysis
     self.theta_vector, self.log_n_matrix, self.log_F_matrix = self.get_ddma(x_vector=self.x_vector,
                                                                             y_vector=self.y_vector,
                                                                             order=self.order,
                                                                             make_intergration=self.make_integration,
                                                                             n_angles=self.n_angles,
                                                                             refresh=self.refresh)
   ```
   
   * As the next step of analysis, we fit the linear regression for every fluctuation function obtained for each direction, within the range of scales defined with `start_scale` and `end_scale` parameters. And then, we obtain the slope of the fitted line which corresponds to the Hurst exponent ( &alpha; ), thus getting &alpha;(&theta;) scaling exponent dependency over angle &theta;.    
   
   ```
     # get angle dependence of the slope in the range [self.start_scale, self.end_scale]
     self.slope_theta = self.get_angle_dependence_of_slope_in_range(start_scale=self.start_scale,
                                                                  end_scale=self.end_scale) 
   ``` 

   * Afterwards we obtain the min and max points on the &alpha;(&theta;) graph, thus obtaining the orientations orthogonal to the main components. 
   
   ```
   # get angles orthogonal to original orientation angles of components
        self.comp1.theta_hat, self.comp2.theta_hat = self.__calculate_independent_components_orthogonal_orientations(aplha_theta_vector=self.slope_theta)
   ```
   
   * As the next step, we obtain the orientations of the main components by subtracting 90&deg; from each direction.   
   
   ```
    # get angles of orientations of independent components
    self.comp1.theta, self.comp2.theta = self.__calculate_independent_components_orientations(theta1_hat=self.comp1.theta_hat,
                                                                                                  theta2_hat=self.comp2.theta_hat)
   ``` 
   
   * Then, we obtain the components along the directions obtained in the previous step, using the following formulas:
   
   &epsilon;<sub>1</sub> = &Chi; * (sin(&theta;<sub>2</sub>) / sin(&theta;<sub>2</sub> - &theta;<sub>1</sub>)) + &Upsilon; * ( cos(&theta;<sub>2</sub>) / sin(&theta;<sub>1</sub> - &theta;<sub>2</sub>))
   
   &epsilon;<sub>2</sub> = &Chi; * (sin(&theta;<sub>1</sub>) / sin(&theta;<sub>1</sub> - &theta;<sub>2</sub>)) + &Upsilon; * ( cos(&theta;<sub>1</sub>) / sin(&theta;<sub>2</sub> - &theta;<sub>1</sub>))
   
   ```
    # get independent components
    self.comp1.ts, self.comp2.ts = self.get_independent_component_vectors(x_vector=self.x_vector, theta1=self.comp1.theta,
                                                                              y_vector=self.y_vector, theta2=self.comp2.theta)
   ```
  
   * Afterwards, we perform [DMA (Detrended Moving Average)](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/cdma/cdma.c) analysis of the obtained components, thus getting their log-log plots to define their scaling exponent features.
   
  ```
    # get DMA analysis of independent components
    self.comp1.log_s, self.comp1.log_F = self.get_cdma(x_vector=self.comp1.ts, order=self.order, make_intergration=self.make_integration)
    self.comp2.log_s, self.comp2.log_F = self.get_cdma(x_vector=self.comp2.ts, order=self.order, make_intergration=self.make_integration)
  ```
    
   * Finally, we obtain Hurst exponents of components within the same scaling range, defined by [start_idx:stop_idx]
   
   ```
     # get Hurst exponent of independent components in log_s[start_scale:end_scale] range
     str_idx, _ = self.get_the_closest_value(list=self.comp1.log_s, value=self.start_scale)
     end_idx, _ = self.get_the_closest_value(list=self.comp1.log_s, value=self.end_scale)
     self.comp1.alpha, _ = self.__calculate_scaling_exponent_in_range(self.comp1.log_s, self.comp1.log_F, start_scale=str_idx, end_scale=end_idx)
     self.comp2.alpha, _ = self.__calculate_scaling_exponent_in_range(self.comp2.log_s, self.comp2.log_F, start_scale=str_idx, end_scale=end_idx)
   ```

   


## Examples
To run the examples create new python file in any working directory.
 
### CoP examples
#### [Backward-leaning example](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/examples/cop_examples.py#L79)
```
import ofsca.examples.cop_examples as ofsca_cop_examples
import matplotlib.pyplot as plt
fig = ofsca_cop_examples.example_backward_leaning()
plt.tight_layout()
plt.show()
```

#### [Quite standing example](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/examples/cop_examples.py#L102)
```
import ofsca.examples.cop_examples as ofsca_cop_examples
import matplotlib.pyplot as plt
fig = ofsca_cop_examples.example_standing()
plt.tight_layout()
plt.show()
```

### [Earthquake example](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/examples/earthquake_example.py#L51)
```
import ofsca.examples.earthquake_example as ofsca_earthquake_example
import matplotlib.pyplot as plt
fig = ofsca_earthquake_example.earthquake_example()
plt.tight_layout()
plt.show()
```

### [Generated time series example](https://gitlab.com/IvanSeleznov/ofsca/-/blob/v1.1/ofsca/examples/numerical_example.py#L29)
```
import ofsca.examples.numerical_example as ofsca_numerical_example
import matplotlib.pyplot as plt
fig = ofsca_numerical_example.example_numerical()
plt.tight_layout()
plt.show()
```

## refresh param

![alt text](i_refresh.png "Title")

This figure demonstrates the importance of i_refresh parameter.

All log-log plots are obtained for 4th order DMA

The figures show the data length dependence(N is the time series length).
 
1) (a) - corresponds to N = 10000 points
2) (b) - corresponds to N = 100000 points
3) (c) - corresponds to N = 1000000 points

If you obtain log-log plot which is destorted the following way(red plots on panel (b) and (c)), please adjust i_refresh parameter. 

