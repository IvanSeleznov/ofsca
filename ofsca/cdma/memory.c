/** @file memory.c
 *  @brief Helper functions to work with memory
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#include "cdma.h"
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"

/* The functions below are based on those of the same names in Numerical
   Recipes (see above). */
void error(char error_text[])
{
    PyErr_Format(PyExc_MemoryError, "Cannot allocate memory in %s!", error_text);
    PyErr_Print();
    exit(1);
}

double** matrix2d(int nrows, int ncolumns)
/* allocate a 2d matrix with shape (nrows, ncolumns) */
{
	int i;
	double **mtrx = malloc(nrows * sizeof(double *));
	for(i = 0; i < nrows; i++)
		mtrx[i] = malloc(ncolumns * sizeof(double));
	return mtrx;
}

double *vector(long nl, long nh)
/* allocate a double vector with subscript range v[nl..nh] */
{
    double *v = (double *)malloc((size_t)((nh-nl+2) * sizeof(double)));
    if (v == NULL) error("allocation failure in vector()");
    return (v-nl+1);
}

int *ivector(long nl, long nh)
/* allocate an int vector with subscript range v[nl..nh] */
{
    int *v = (int *)malloc((size_t)((nh-nl+2) * sizeof(int)));
    if (v == NULL) error("allocation failure in ivector()");
    return (v-nl+1);
}

long *lvector(long nl, long nh)
/* allocate a long int vector with subscript range v[nl..nh] */
{
    long *v = (long *)malloc((size_t)((nh-nl+2) * sizeof(long)));
    if (v == NULL) error("allocation failure in lvector()");
    return (v-nl+1);
}

void free_vector(double *v, long nl, long nh)
/* free a double vector allocated with vector() */
{
    free(v+nl-1);
}

void free_ivector(int *v, long nl, long nh)
/* free an int vector allocated with ivector() */
{
    free(v+nl-1);
}

void free_lvector(long *v, long nl, long nh)
/* free a long int vector allocated with lvector() */
{
    free(v+nl-1);
}

double *pyvector_to_Carrayptrs(PyArrayObject *arrayin)
{
    return (double *) PyArray_DATA(arrayin);
}

double **ptrvector(long n)
{
    double **v;
    v=(double **)malloc((size_t) (n*sizeof(double)));
    if (!v)
    {
        printf("In **ptrvector. Allocation of memory for double array failed.");
        exit(0);
    }
 	return v;
}

double **pymatrix_to_Carrayptrs(PyArrayObject *arrayin)  {
    double **c, *a;
 	int i,n,m;

    n=PyArray_DIMS(arrayin)[0];
    m=PyArray_DIMS(arrayin)[1];

	c=ptrvector(n);
	a=(double *) PyArray_DATA(arrayin);

	for (i=0; i<n; i++)
	{
		c[i]=a+i*m;
	}
    return c;
}



