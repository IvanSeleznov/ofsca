/** @file cDMA0.c
 *  @brief Fast algorithm for CDMA0
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

#include "cdma.h"

void cdma0(pyarray_cdma_result_t *output, double *data, int n, int minbox, int maxbox, int integ, int ends, int refresh)
{
	double *F2;
	long i;
	double *y; /* integrated data */
	long nr;	/* number of box sizes */
    double yave;

	clock_t start, end;
	i_refresh = refresh;

	printf("Total data length = %d\n", n);
	printf("***** cDMA0 *****\n");

    /*Intergrate data*/
    y = vector(1,n);
	if(integ > 0){
	  /* calulation of mean */
	  yave = 0;
	  for(i=1;i<=n;i++){
	    yave += data[i];
	  }
	  yave = yave/(double)n;

	  /* integration */

	  y[1] = (data[1]-yave);
	  for(i=2;i<=n;i++){
	    y[i] = y[i-1]+(data[i]-yave);
	  }
	}else{
	  for(i=1;i<=n;i++){
	    y[i] = data[i];
	  }
	}

	/* Set minimum and maximum box sizes. */
	minbox = (long)(minbox/2)*2+1; /* minbox should be an odd number */
	if(minbox < 3) minbox = 3;
	if(maxbox > n/4 || minbox > maxbox) maxbox = (long)n/4;
	
	/* "rscale" is based on that of "dfa.c".      */
	/* https://www.physionet.org/physiotools/dfa/ */
	/* Note that scales are odd numbers in CDMA.  */
	nr = rscale_cdma0(minbox, maxbox, pow(2.0, 1.0/8.0));

    /*create C struct for results and assign vector shapes*/
    cstruct_cdma_result_t result;
    result.scale_arr_len[0] = nr;
    result.F_arr_len[0] = nr;
	/*link C struct to the PyArray results struct*/
    Pyarray_to_Cstruct_cdma_result(output, &result);

	/* For measurement of processing time */
	start = clock();
	printf("starting time:%ld\n", start);
	
	/* define fluctuation function */
	F2 = vector(1,nr);	

	/* estimation of squared deviations */
	for(i=1;i<=nr;i++){
		F2[i] = est_f2_cdma0(data, y, n, rs[i], nr, i_refresh, ends);
		result.F_vector[i-1] = log(F2[i])/log(10)/2;
		result.scale_v[i-1] = log((double)rs[i]/1.00)/log(10);
	}

	/* For measurement of processing time */
	end = clock();
	printf("Finish time: %ld\n", end);
	printf("Processing time: %ld[ms]\n", (end - start));

	/* output estimated results */
//	if(nr > 1){
//		for(i=1;i<=nr;i++){
//			fprintf(stdout,"%lf,%lf,%lf\n",log((double)rs[i])/log(10),log(F2[i])/log(10)/2,log((double)rs[i]/1.00)/log(10));
//		}
//	}else{
//		for(i=1;i<=n;i++){
//			fprintf(stdout,"%lf,%lf\n",y[i],data[i]);
//		}
//	}

	/* Release allocated memory. */
	free_vector(y,1,n);
	free_vector(F2,1,nr);
}

/* calculation of coefficients */
double cmat_cdma0(double k)
{
	return 1/(2*k+1);
}

double sg0(long m, long n)
{
	return 1/(double)((n-1)/2 + m + 1);
}


double est_f2_cdma0(double *data, double *y, long n, long scale, long nr, int i_refresh, int ends)
{
	double f2,a0,a0L,a0R;
	double y00,y10;
	double temp,temp1,temp2,y0L,y0R;
	long i,j; /* n: data length */
	long itemp;
	long k,ic;

	double c1;

	f2 = 0;
	k = (scale-1)/2;
	ic = k+1;
	f2 = 0;

	if(ends==1){
	  /* both ends */
	  y0L=0;
	  y0R=0;
	  for(i=1;i<=k;i++){
	    y0L += y[i];
	    y0R += y[n-i+1];
	  }

	  for(i=1;i<=k;i++){
	    y0L += y[k+i];
	    y0R += y[n-i+1-k];

	    c1 = sg0(i-1 ,scale);
	    a0L = c1*y0L;
	    a0R = c1*y0R;
/******************************/
	  if(nr == 1){
		data[i] = a0L;
		data[n-i+1] = a0R;
	  }
/******************************/
	    temp1 = (y[i] - a0L);
	    temp2 = (y[n-i+1] - a0R);
	    f2 += temp1*temp1+temp2*temp2;
	  }
	}
	/* initial values */
	y10=0;
	for(i=1;i<=scale;i++){
		y10 += y[i];
	}

	c1 = cmat_cdma0((double)k);

	a0 = c1*y10;
	temp = (y[ic] - a0);

/******************************/
	  if(nr == 1){
		data[ic] = a0;
	  }
/******************************/

	f2 = temp*temp;

	itemp = n-scale;

	for(i=1;i<=itemp;i++){
		if(i % i_refresh == 0){
			y10=0;
			for(j=1;j<=scale;j++){
				y10 += y[i+j];
			}
		}else{
			y00 = y10;
			y10 = y00 + y[i+scale]-y[i];
		}
		
		a0 = c1*y10;
/******************************/
	if(nr == 1){
		data[i+ic] = a0;
	}
/******************************/

		temp = (y[i+ic] - a0);
		f2 += temp*temp;
	}
	if(ends==1){
	  return f2/(double)(itemp+1);
	}else{
	  return f2/(double)(n);
	}
}

/* This function is based on that of the same names in dfa.c. */
int rscale_cdma0(long minbox, long maxbox, double boxratio)
{
    long ir, n;
    long rw;
    long rslen;	/* length of rs[] */

	/*  */
    rslen = log10(maxbox /minbox) / log10(boxratio) + 1;
    rs = lvector(1, rslen);
    for (ir = 1, n = 2, rs[1] = minbox; n <= rslen && rs[n-1] < maxbox; ir++)
      if ((rw = minbox * pow(boxratio, ir) + 0.5) > rs[n-1])
            rs[n++] = (long)(rw/2)*2+1;
    if (rs[--n] > maxbox) --n;
    return (n);
}