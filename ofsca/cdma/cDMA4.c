/** @file cDMA4.c
 *  @brief Fast algorithm for CDMA4
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

#include "cdma.h"

/* Global variables */
long *rs;
double c1,c2,c3;
double *loc_sum0,*loc_sum1,*loc_sum2,*loc_sum3,*loc_sum4;

void cdma4(pyarray_cdma_result_t *output, double *data, int n, int minbox, int maxbox, int integ, int ends, int refresh)
{
	double *F2;
	long i;
	double *y; /* input data and integrated data */
    long nr;	/* number of box sizes */
    double yave;

	clock_t start, end;

	/* default values for selected data column */
	i_refresh = 500;

	/* read data file */
	printf("Total data length = %d\n", n);
	printf("***** cDMA4 *****\n");
    y = vector(1,n);
	/* calulation of mean */
	yave = 0;
	for(i=1;i<=n;i++){
		yave += data[i];
	}
	yave = yave/(double)n;

	if(integ > 0){
	  /* calulation of mean */
	  yave = 0;
	  for(i=1;i<=n;i++){
		yave += data[i];
	  }
	  yave = yave/(double)n;

	  /* integration */

	  y[1] = (data[1]-yave);
	  for(i=2;i<=n;i++){
		y[i] = y[i-1]+(data[i]-yave);
	  }
	}else{
	  for(i=1;i<=n;i++){
		y[i] = data[i];
	  }
	}

	if(i_refresh > n) i_refresh=n;
	loc_sum0 = vector(1,(long)(n/i_refresh)+1);
	loc_sum1 = vector(1,(long)(n/i_refresh)+1);
	loc_sum2 = vector(1,(long)(n/i_refresh)+1);
	loc_sum3 = vector(1,(long)(n/i_refresh)+1);
	loc_sum4 = vector(1,(long)(n/i_refresh)+1);
	
	/* Set minimum and maximum box sizes. */
	minbox = (long)(minbox/2)*2+1; /* minbox should be an odd number */
	if(minbox < 5) minbox = 5;
	if(maxbox > n/3 || minbox > maxbox) maxbox = (long)n/3;
	
	/* "rscale" is based on that of "dfa.c". */
	/* Note that scales are odd numbers in CDMA. */
	nr = rscale_cdma4(minbox, maxbox, pow(2.0, 1.0/8.0));

    /*create C struct for results and assign vector shapes*/
    cstruct_cdma_result_t result;
    result.scale_arr_len[0] = nr;
    result.F_arr_len[0] = nr;
	/*link C struct to the PyArray results struct*/
    Pyarray_to_Cstruct_cdma_result(output, &result);

	/* For measurement of processing time */
	start = clock();
	printf("starting time:%ld\n", start);
	
	/* define fluctuation function */
	F2 = vector(1,nr);	

	/* estimation of squared deviations */
	for(i=1;i<=nr;i++){
		F2[i] = est_f2_cdma4(y, n, i, i_refresh);
		result.F_vector[i-1] = log(F2[i])/log(10)/2;
		result.scale_v[i-1] = log((double)rs[i]/1.00)/log(10);
	}

	/* For measurement of processing time */
	end = clock();
	printf("Finish time: %ld\n", end);
	printf("Processing time: %ld[ms]\n", (end - start));

	/* output estimated results */
//	for(i=1;i<=nr;i++){
//		fprintf(stdout,"%lf,%lf,%lf\n",log((double)rs[i])/log(10),log(F2[i])/log(10)/2,log((double)rs[i]/2.74)/log(10));
//	}

	/* Release allocated memory. */
	free_vector(y,1,n);
	free_vector(F2,1,nr);
	free_vector(loc_sum0,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum1,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum2,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum3,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum4,1,(long)(n/i_refresh)+1);

}

/* calculation of coefficients */
void cmat_cdma4(double k)
{
	double k2,k3,k4,den;

	k2 = k*k;
	k3 = k2*k;
	k4 = k3*k;
	den = 180 + 72*k - 800*k2 - 320*k3 + 320*k4 + 128* k*k4;
	c1 = (180 - 750*k - 525*k2 + 450*k3 + 225*k4) / den;
	c2 = (1575 - 1050*k - 1050*k2) / den;
    c3 = 945/ den;
}


double est_f2_cdma4(double *y, long n, long k2, int i_refresh)
{
	double f2,a0;
	double y00,y0i,y0ii,y0iii,y0iv,y10,y1i,y1ii,y1iii,y1iv;
	double temp1,temp2;
	double nn1,nn2,nn3,nn4;
	long i,j,i0; /* n: data length */
	long itemp;
	long k,kb,ic,scale,iloc;
	
	scale = rs[k2];
	k = (scale-1)/2;
	ic = k+1;

	/* initial values */
	iloc=1;
	kb = (rs[k2-1]-1)/2;
	nn1=(double)(kb-k);
	nn2=nn1*nn1;
	nn3=nn2*nn1;
	nn4=nn2*nn2;
	
	if(k2 == 1){
		i0 = 1;
		y10=0;
		y1i=0;
		y1ii=0;
		y1iii=0;
		y1iv=0;
	}else{
		i0 = rs[k2-1]+1;
		y10=loc_sum0[iloc];
		y1i=loc_sum1[iloc]+loc_sum0[iloc]*nn1;
		y1ii=loc_sum2[iloc]+2*loc_sum1[iloc]*nn1+loc_sum0[iloc]*nn2;
		y1iii=loc_sum3[iloc]+3*loc_sum2[iloc]*nn1+3*loc_sum1[iloc]*nn2+loc_sum0[iloc]*nn3;
		y1iv=loc_sum4[iloc]+4*loc_sum3[iloc]*nn1+6*loc_sum2[iloc]*nn2+4*loc_sum1[iloc]*nn3+loc_sum0[iloc]*nn4;
	}

	for(i=i0;i<=scale;i++){
		y10 += y[i];
		temp1 = y[i]*(double)(i-k-1);
		y1i += temp1;
		temp2 = temp1*(double)(i-k-1);
		y1ii += temp2;
		temp1 = temp2*(double)(i-k-1);
		y1iii += temp1;
		temp2 = temp1*(double)(i-k-1);
		y1iv += temp2;
	}
	
	loc_sum0[iloc]=y10;
	loc_sum1[iloc]=y1i;
	loc_sum2[iloc]=y1ii;
	loc_sum3[iloc]=y1iii;
	loc_sum4[iloc]=y1iv;
	
	
	cmat_cdma4((double)k);
	
	a0 = c1*y10+c2*y1ii+c3*y1iv;
	temp1 = (y[ic] - a0);
	f2 = temp1*temp1;

	itemp = n-scale;

	for(i=1;i<=itemp;i++){
		if(i % i_refresh == 0){
			iloc++;
			if(k2 == 1){
				i0 = 1;
				y10=0;
				y1i=0;
				y1ii=0;
				y1iii=0;
				y1iv=0;
			}else{
				i0 = rs[k2-1]+1;
				y10=loc_sum0[iloc];
				y1i=loc_sum1[iloc]+loc_sum0[iloc]*nn1;
				y1ii=loc_sum2[iloc]+2*loc_sum1[iloc]*nn1+loc_sum0[iloc]*nn2;
				y1iii=loc_sum3[iloc]+3*loc_sum2[iloc]*nn1+3*loc_sum1[iloc]*nn2+loc_sum0[iloc]*nn3;
				y1iv=loc_sum4[iloc]+4*loc_sum3[iloc]*nn1+6*loc_sum2[iloc]*nn2+4*loc_sum1[iloc]*nn3+loc_sum0[iloc]*nn4;
			}
			
			for(j=i0;j<=scale;j++){
				y10 += y[i+j];
				temp1 = y[i+j]*(double)(j-k-1);
				y1i += temp1;
				temp2 = temp1*(double)(j-k-1);
				y1ii += temp2;
				temp1 = temp2*(double)(j-k-1);
				y1iii += temp1;
				temp2 = temp1*(double)(j-k-1);
				y1iv += temp2;
			}
			loc_sum0[iloc]=y10;
			loc_sum1[iloc]=y1i;
			loc_sum2[iloc]=y1ii;
			loc_sum3[iloc]=y1iii;
			loc_sum4[iloc]=y1iv;
		}else{
			y00 = y10;
			y0i = y1i;
			y0ii = y1ii;
			y0iii = y1iii;
			y0iv = y1iv;

			y10 = y00 + y[i+scale]-y[i];
			temp1 = y[i]*(double)(k+1);
			temp2 = y[i+scale]*(double)k;
			y1i = y0i - y00 + temp1 + temp2;
			temp1 = temp1*(double)(k+1);
			temp2 = temp2*(double)k;
			y1ii = y0ii - 2*y0i + y00 - temp1 + temp2;
			temp1 = temp1*(double)(k+1);
			temp2 = temp2*(double)k;
			y1iii = y0iii - 3*y0ii + 3*y0i - y00 + temp1 + temp2;
			y1iv = y0iv - 4*y0iii + 6* y0ii - 4 * y0i + y00 - temp1 *(double)(k+1) + temp2*(double)k;
		}
		
		a0 = c1*y10+c2*y1ii+c3*y1iv;

		temp1 = (y[i+ic] - a0);
		f2 += temp1*temp1;
	}

	return f2/(double)(itemp+1);
}

/* This function is based on that of the same names in dfa.c. */
int rscale_cdma4(long minbox, long maxbox, double boxratio)
{
    long ir, n;
    long rw;
    long rslen;	/* length of rs[] */

	/*  */
    rslen = log10(maxbox /minbox) / log10(boxratio) + 1;
    rs = lvector(1, rslen);
    for (ir = 1, n = 2, rs[1] = minbox; n <= rslen && rs[n-1] < maxbox; ir++)
      if ((rw = minbox * pow(boxratio, ir) + 0.5) > rs[n-1])
            rs[n++] = (long)(rw/2)*2+1;
    if (rs[--n] > maxbox) --n;
    return (n);
}