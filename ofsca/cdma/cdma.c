/** @file cdma.c
 *  @brief Main file of Centered DMA Python wrapper
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <Python.h>
#include "cdma.h"

#define DEFAULT_REFRESH_VALUE 666

void Pyarray_to_Cstruct_cdma_result(pyarray_cdma_result_t *py_array_input, cstruct_cdma_result_t *c_struct_output)
{
    /*converting Py_array F_vector to C pointer type vector*/
    py_array_input->F_v = PyArray_SimpleNew(1, c_struct_output->F_arr_len, NPY_DOUBLE);
    c_struct_output->F_vector = pyvector_to_Carrayptrs(py_array_input->F_v); // converting allocated memory to C ptr

    /*converting Py_array scale_vector to C pointer type vector*/
    py_array_input->scale_v = PyArray_SimpleNew(1, c_struct_output->scale_arr_len, NPY_DOUBLE);
    c_struct_output->scale_v = pyvector_to_Carrayptrs(py_array_input->scale_v); // converting allocated memory to C ptr
}

static PyObject *get_cDMA(PyObject *self, PyObject* args, PyObject* kwargs) {
    
    static char* keywords[] = { "data",
                                //optional
                                "minbox",
                                "maxbox", 
                                "integrate", 
                                "asym_intervals",
                                "order",
                                "refresh",
                                NULL};

    /** Default values */
    int minbox = 7;
    int maxbox = 0;
    int integ = 1;
    int ends = 0; /* using only symmetric intervals */
    int order = 0;
    int refresh = DEFAULT_REFRESH_VALUE;
    
    //input arrays
    PyArrayObject *vecin;
    PyObject *data_obj=NULL;
    double *data_buf;

    pyarray_cdma_result_t cdma_result; /*variable for result structure*/

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,
                                     "O|iiiiii",
                                     keywords,
                                     &vecin,
                                     &minbox,
                                     &maxbox,
                                     &integ,
                                     &ends,
                                     &order,
                                     &refresh
                                     )) {
        return NULL;
    }

    if (NULL == vecin)  return NULL;

    data_obj = PyArray_FROM_OTF(vecin, NPY_DOUBLE, NPY_ARRAY_IN_ARRAY);
    if (data_obj == NULL) return NULL;

    int n1=PyArray_DIMS(data_obj)[0];
    data_buf=pyvector_to_Carrayptrs(data_obj);

    switch(order) {
        case 0:
            if(refresh == DEFAULT_REFRESH_VALUE) refresh = 50000;
            printf("refresh: %i\n", refresh);
            cdma0(&cdma_result, data_buf, n1, minbox, maxbox, integ, ends, refresh);
            break;
        case 2:
            if(refresh == DEFAULT_REFRESH_VALUE) refresh = 5000;
            printf("refresh: %i\n", refresh);
            cdma2(&cdma_result, data_buf, n1, minbox, maxbox, integ, ends, refresh);
            break;
        case 4:
            if(refresh == DEFAULT_REFRESH_VALUE) refresh = 500;
            printf("refresh: %i\n", refresh);
            cdma4(&cdma_result, data_buf, n1, minbox, maxbox, integ, ends, refresh);
            break;
        default:
            PyErr_SetString(PyExc_ValueError, "order should be 0/2/4 integer value!");
            return NULL;
    }

    PyObject *rslt = PyTuple_New(2);
    PyTuple_SetItem(rslt, 0, cdma_result.scale_v);
    PyTuple_SetItem(rslt, 1, cdma_result.F_v);

    return rslt;
}

static PyMethodDef methods[] = {
    {
        "get_cDMA",                                     // computes centered DMA
        (PyCFunction) get_cDMA,                         // C wrapper function
        METH_VARARGS | METH_KEYWORDS,                   // received variable args (but really just 1)
        "Computes Centered Detrended Moving Average."   // documentation
    }, 
    {NULL}
};

static struct PyModuleDef cdma = {
    PyModuleDef_HEAD_INIT,
    "cdma",                                                                 // name of module exposed to Python
    "C extention module that computes Centered Detrended Moving Average",   // module documentation
    -1,
    methods
};

PyMODINIT_FUNC PyInit_cdma(void) {
    import_array();
    return PyModule_Create(&cdma);
}

#ifdef __cplusplus
}  // extern "C"
#endif