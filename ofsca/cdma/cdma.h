/** @file cdma.h
 *  @brief Header file of Centered DMA Python wrapper
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#ifndef CDMA_H
#define CDMA_H

#include <stdio.h>
#include <stdlib.h>
#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"

#ifdef __linux__
#include <string.h>
#define strcat_s(arg1, sizeof_arg1, arg2) strcat(arg1, arg2)
#define strcpy_s(arg1, sizeof_arg1, arg2) strcpy(arg1, arg2)
#endif

#define SWAP(a,b) {temp = (a); (a) = (b); (b) = temp;}

long i_refresh;
long *rs;

/** @brief Structure for holding results of cDMA in C array objects */
typedef struct c_cdma_result_s {
    double *F_vector;	 			/*fluctuation function vector */
    npy_intp F_arr_len[2];

    double *scale_v; 			/*vector of scale*/
    npy_intp scale_arr_len[2];
} cstruct_cdma_result_t;

/** @brief Structure for holding results of cDMA in py array objects */
typedef struct cdma_result_s {
    PyArrayObject *F_v;
    PyArrayObject *scale_v;
} pyarray_cdma_result_t;

/** @brief Computes 0-th order Centered DMA for 1D vector.
 *  @param *output Pointer to output structure of type pyarray_ddma_result_t
 *  @param *data Pointer to the input vector
 *  @param n Length of input vector data
 *  @param minbox Smallest box width
 *  @param maxbox Largest box width
 *  @param integ Apply integration
 *  @param ends Use all asymmetric intervals
 *  @param refresh parameter to get rid of the summation error
 *
 *  @return Void
 */
void cdma0(pyarray_cdma_result_t *output, double *data, int n, int minbox, int maxbox, int integ, int ends, int refresh);

/** @brief Computes 2-nd order Centered DMA for 1D vector.
 *  @param *output Pointer to output structure of type pyarray_ddma_result_t
 *  @param *data Pointer to the input vector
 *  @param n Length of input vector data
 *  @param minbox Smallest box width
 *  @param maxbox Largest box width
 *  @param integ Apply integration
 *  @param ends Use all asymmetric intervals
 *  @param refresh parameter to get rid of the summation error
 *
 *  @return Void
 */
void cdma2(pyarray_cdma_result_t *output, double *data, int n, int minbox, int maxbox, int integ, int ends, int refresh);

/** @brief Computes 4-th order Centered DMA for 1D vector.
 *  @param *output Pointer to output structure of type pyarray_ddma_result_t
 *  @param *data Pointer to the input vector
 *  @param n Length of input vector data
 *  @param minbox Smallest box width
 *  @param maxbox Largest box width
 *  @param integ Apply integration
 *  @param ends Use all asymmetric intervals
 *  @param refresh parameter to get rid of the summation error
 *
 *  @return Void
 */
void cdma4(pyarray_cdma_result_t *output, double *data, int n, int minbox, int maxbox, int integ, int ends, int refresh);

/** @brief Function for printing put error message
 *  @param error_text[] error string
 *  @return Void
 */
void error(char error_text[]);


/** Functions to calculate 0-th order cDMA */
double sg0(long m, long n);
double est_f2_cdma0(double *data, double *y, long n, long scale, long nr, int i_refresh, int ends);
double cmat_cdma0(double k);
int rscale_cdma0(long minbox, long maxbox, double boxratio);

/** Functions to calculate 2-nd order cDMA */
void sg2(double *c0, long m, long n);
double est_f2_cdma2(double *data, double *y, long nr, long n, long k2, int ends, int i_refresh);
void cmat_cdma2(double k);
int rscale_cdma2(long minbox, long maxbox, double boxratio);

/** Functions to calculate 4-th order cDMA */
double est_f2_cdma4(double *y, long n, long k2, int i_refresh);
void cmat_cdma4(double k);
int rscale_cdma4(long minbox, long maxbox, double boxratio);

/** Functions to manipulate memory*/
double *vector(long nl, long nh);
int *ivector(long nl, long nh);
long *lvector(long nl, long nh);
void free_vector(double *v, long nl, long nh);
void free_ivector(int *v, long nl, long nh);
void free_lvector(long *v, long nl, long nh);
double **matrix2d(int nrows, int ncolumns);

/** Functions to manipulate memory*/
/** @brief Function for getting C pointer from py array*/
double *pyvector_to_Carrayptrs(PyArrayObject *arrayin);
/** @brief Function for getting C pointer from py array*/
double **ptrvector(long n);
/** @brief Function for converting py matrix to C double pointer matrix*/
double **pymatrix_to_Carrayptrs(PyArrayObject *arrayin);
/** @brief Function for converting C results to Py array result*/
void Pyarray_to_Cstruct_cdma_result(pyarray_cdma_result_t *py_array_input, cstruct_cdma_result_t *c_struct_output);

#endif /*CDMA_H*/