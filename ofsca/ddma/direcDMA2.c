/** @file direcDMA2.c
 *  @brief Directionally dependent DMA analysis of 2-nd order
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

#include "ddma.h"

/* Global variables */
long *rs;
double c1,c2;
double *loc_sum0,*loc_sum1,*loc_sum2;

void ddma2(pyarray_ddma_result_t *output, double *data1, double *data2, int n, int n_angl, int minbox, int maxbox, int integ, int ends, int refresh)
{
	double *F2,theta,d_theta;
	double *x_d,mu_x;
	long i,j,k;
	clock_t start, end;
    long nr;	/* number of box sizes */

    double *y;

    i_refresh = refresh;

	printf("Total data length = %i\n", n);
    printf("***** DDMA2 *****\n");

	if(i_refresh > n) i_refresh=n;
	loc_sum0 = vector(1,(long)(n/i_refresh)+1);
	loc_sum1 = vector(1,(long)(n/i_refresh)+1);
	loc_sum2 = vector(1,(long)(n/i_refresh)+1);

	/* Set minimum and maximum box sizes. */
	minbox = (long)(minbox/2)*2+1; /* minbox should be an odd number */
	if(minbox < 5) minbox = 5;
	if(maxbox > n/3 || minbox > maxbox) maxbox = (long)n/3;

	/* "rscale" is based on that of "dfa.c". */
	/* Note that scales are odd numbers in CDMA. */
	nr = rscale_ddma2(minbox, maxbox, pow(2.0, 1.0/8.0));

    /*create C struct for results and assign vector and mtrx shapes*/
    cstruct_ddma_result_t result;
    result.theta_arr_len[0] = n_angl;
    result.scale_arr_len[0] = nr;
    result.F_mtrx_shape[0] = n_angl;
	result.F_mtrx_shape[1] = nr;
	/*link C struct to the PyArray results struct*/
    Pyarray_to_Cstruct_ddma_result(output, &result);

	/* For measurement of processing time */
	start = clock();
	printf("starting time:%ld\n", start);
	
	/* define fluctuation function */
	F2 = vector(1,nr);	

	x_d = vector(1,n);
	y = vector(1,n);
	d_theta = 3.141592653589793/(double)n_angl;

	for(i=1;i<=n_angl;i++){
		theta = d_theta*((double)i-1.0);

		/*save to results vector*/
		result.theta_v[i-1] = theta;
//		printf("Theta(%ld): %lf\n",i, theta);
		
		mu_x = 0;
		for(j=1;j<=n;j++){
			x_d[j] = data1[j]*cos(theta) + data2[j]*sin(theta);
			mu_x += x_d[j];
		}
		
		mu_x = mu_x/(double)n;
		
		if(integ > 0){
		/* integration */
			y[1] = (x_d[1]-mu_x);
			for(k=2;k<=n;k++){
				y[k] = y[k-1]+(x_d[k]-mu_x);
			}
		}else{
			for(k=1;k<=n;k++){
				y[k] = x_d[k]-mu_x;
			}
		}
			/* estimation of squared deviations */
		for(k=1;k<=nr;k++){
			F2[k] = est_f2_ddma2(y, n, k, ends, i_refresh);
			/* output estimated results */
			result.F_mtrx[i-1][k-1] = log(F2[k])/log(10)/2;
			result.scale_v[k-1] = log((double)rs[k]/1.93)/log(10);
//			printf("%ld,%lf,%lf,%lf,%lf\n",i,theta,log((double)rs[k])/log(10),log(F2[k])/log(10)/2,log((double)rs[k]/1.93)/log(10));
		}
	}

    /* For measurement of processing time */
	end = clock();
	printf("Finish time: %ld\n", end);
	printf("Processing time: %ld[ms]\n", (end - start));

	/* Release allocated memory. */
	free_vector(y,1,n);
	free_vector(x_d,1,n);
	free_vector(F2,1,nr);
	free_vector(loc_sum0,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum1,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum2,1,(long)(n/i_refresh)+1);

	return;
}

/* calculation of coefficients */
void cmat_ddma2(double k)
{
	double k2,den;

	k2 = k*k;
	den = 8*k2*k+12*k2-2*k-3;

	c1 = (9*k2+9*k-3)/den;
	c2 = -15/den;
}

void sg2(double *c0, long m, long n)
{
	double d;
	double mn,n2,n3,m2,m3,k2;
	long k;

	mn = 2*(double)m+(double)n;
	n2 = n*n;
	n3 = n2*n;
	m2 = m*m;
	m3 = m2*m;

	d = (mn+3)*(mn-3)*(mn-1)*(mn+1)*(mn+5);

	mn = m*n;

	for(k=-m;k<=(n-1)/2;k++){
		k2 = k*k;
		c0[k] = (90 + 720*k2 - 240*m + 576*k*m + 960*k2*m + 48*m2 + 1728*k*m2 +
			960*k2*m2 + 576*m3 + 1152*k*m3 + 288*m2*m2 - 120*n - 432*k*n -
			960*k2*n - 528*mn - 2304*k*mn - 1920*k2*mn - 864*m2*n -
			2304*k*m2*n - 576*m3*n + 84*n2 + 576*k*n2 + 240*k2*n2 +
			720*m*n2 + 1152*k*m*n2 + 720*m2*n2 - 72*n3 - 144*k*n3 -
			144*m*n3 + 18*n2*n2)/d;
	}
}

double est_f2_ddma2(double *y, long n, long k2, int ends, int i_refresh){
	double f2,a0,a0L,a0R;
	double y00,y0i,y0ii,y10,y1i,y1ii;
	double temp1,temp2;
	double nn1,nn2;
	double *c0_;
	long i,j,i0; /* n: data length */
	long itemp;
	long k,kb,ic,scale,iloc;

	scale = rs[k2];
	k = (scale-1)/2;
	ic = k+1;
	f2 = 0;

	if(ends==1){
	  /* both ends */
	  c0_ = vector(-k,k);

	  for(i=1;i<=k;i++){
		sg2(c0_, i-1 ,scale);
		a0L = 0;
		a0R = 0;
		for(j=1;j<=i+k;j++){
			a0L += c0_[j-i]*y[j];
			a0R += c0_[j-i]*y[n-j+1];
		}

		temp1 = (y[i] - a0L);
		temp2 = (y[n-i+1] - a0R);
		f2 += temp1*temp1+temp2*temp2;
	  }
	free_vector(c0_,-k,k);
	}
	/* center part*/	
	/* initial values */

	iloc=1;
	kb = (rs[k2-1]-1)/2;
	nn1=(double)(kb-k);
	nn2=nn1*nn1;
	
	if(k2 == 1){
		i0 = 1;
		y10=0;
		y1i=0;
		y1ii=0;
	}else{
		i0 = rs[k2-1]+1;
		y10=loc_sum0[iloc];
		y1i=loc_sum1[iloc]+loc_sum0[iloc]*nn1;
		y1ii=loc_sum2[iloc]+2*loc_sum1[iloc]*nn1+loc_sum0[iloc]*nn2;
	}

	for(i=i0;i<=scale;i++){
		y10 += y[i];
		temp1 = y[i]*(double)(i-k-1);
		y1i += temp1;
		temp2 = temp1*(double)(i-k-1);
		y1ii += temp2;
	}
	loc_sum0[iloc]=y10;
	loc_sum1[iloc]=y1i;
	loc_sum2[iloc]=y1ii;

	cmat_ddma2((double)k);
	a0 = c1*y10+c2*y1ii;
	temp1 = (y[ic] - a0);
	f2 += temp1*temp1;

	itemp = n-scale;
	for(i=1;i<=itemp;i++){
		if(i % i_refresh == 0){
			iloc++;
			if(k2 == 1){
				i0 = 1;
				y10=0;
				y1i=0;
				y1ii=0;
			}else{
				i0 = rs[k2-1]+1;
				y10=loc_sum0[iloc];
				y1i=loc_sum1[iloc]+loc_sum0[iloc]*nn1;
				y1ii=loc_sum2[iloc]+2*loc_sum1[iloc]*nn1+loc_sum0[iloc]*nn2;
			}

			for(j=i0;j<=scale;j++){
				y10 += y[i+j];
				temp1 = y[i+j]*(double)(j-k-1);
				y1i += temp1;
				temp2 = temp1*(double)(j-k-1);
				y1ii += temp2;
			}
			loc_sum0[iloc]=y10;
			loc_sum1[iloc]=y1i;
			loc_sum2[iloc]=y1ii;
		}else{
			y00 = y10;
			y0i = y1i;
			y0ii = y1ii;

			y10 = y00 + y[i+scale]-y[i];
			temp1 = y[i]*(double)(k+1);
			temp2 = y[i+scale]*(double)k;
			y1i = y0i - y00 + temp1 + temp2;
			y1ii = y0ii - 2*y0i + y00 - temp1 *(double)(k+1) + temp2*(double)k;
		}
		a0 = c1*y10+c2*y1ii;

		temp1 = (y[i+ic] - a0);
		f2 += temp1*temp1;
	}
	if(ends==1){
	  return f2/(double)(itemp+1);
	}else{
	  return f2/(double)(n);
	}
}

/* This function is based on that of the same names in dfa.c. */
int rscale_ddma2(long minbox, long maxbox, double boxratio)
{
    long ir, n;
    long rw;
    long rslen;	/* length of rs[] */
	/*  */
    rslen = log10(maxbox /minbox) / log10(boxratio) + 1;
    rs = lvector(1, rslen);
    for (ir = 1, n = 2, rs[1] = minbox; n <= rslen && rs[n-1] < maxbox; ir++)
      if ((rw = minbox * pow(boxratio, ir) + 0.5) > rs[n-1])
            rs[n++] = (long)(rw/2)*2+1;
    if (rs[--n] > maxbox) --n;
    return (n);
}