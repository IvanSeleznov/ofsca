/** @file ddma.h
 *  @brief Header file of Directional DMA Python wrapper
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#ifndef DIRECTED_DMA_H
#define DIRECTED_DMA_H

#include <stdio.h>
#include <stdlib.h>
#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"

//#include "memory.h"

#ifdef __linux__
#include <string.h>
#define strcat_s(arg1, sizeof_arg1, arg2) strcat(arg1, arg2)
#define strcpy_s(arg1, sizeof_arg1, arg2) strcpy(arg1, arg2)
#endif

#define SWAP(a,b) {temp = (a); (a) = (b); (b) = temp;}

long i_refresh;
long *rs;

/** @brief Structure for holding results of Directional DMA in C array objects */
typedef struct angle_ddma_result_s { 
    double *theta_v; 			/*angle theta*/
    npy_intp theta_arr_len[2];

    double **F_mtrx;	 			/*fluctuation function vector */
    npy_intp F_mtrx_shape[2];

    double *scale_v; 			/*vector of scale*/
    npy_intp scale_arr_len[2];
} cstruct_ddma_result_t;

/** @brief Structure for holding results of Directional DMA in py array objects */
typedef struct ddma_result_s {
    PyArrayObject *theta_v;
    PyArrayObject *F_mtrx;
    PyArrayObject *scale_v;
} pyarray_ddma_result_t;

/** @brief Computes 0-th order Directionally dependent DMA for 2D trajectory.
 *
 *  @param *output Pointer to output structure of type pyarray_ddma_result_t
 *  @param *data1 Pointer to X-axis of 2D trajectory
 *  @param *data2 Pointer to Y-axis of 2D trajectory
 *  @param n Length of input vectors data1 and data2
 *  @param n_angl Equally divided angles
 *  @param minbox Smallest box width
 *  @param maxbox Largest box width
 *  @param integ Apply integration
 *  @param ends Use all asymmetric intervals
 *  @param refresh parameter to get rid of the summation error
 *
 *  @return Void
 */
void ddma0(pyarray_ddma_result_t *output, double *data1, double *data2, int n, int n_angl, int minbox, int maxbox, int integ, int ends, int refresh);

/** @brief Computes 2-nd order Directionally dependent DMA for 2D trajectory.
 *
 *  @param *output Pointer to output structure of type pyarray_ddma_result_t
 *  @param *data1 Pointer to X-axis of 2D trajectory
 *  @param *data2 Pointer to Y-axis of 2D trajectory
 *  @param n Length of input vectors data1 and data2
 *  @param n_angl Equally divided angles
 *  @param minbox Smallest box width
 *  @param maxbox Largest box width
 *  @param integ Apply integration
 *  @param ends Use all asymmetric intervals
 *  @param refresh parameter to get rid of the summation error
 *
 *  @return Void
 */
void ddma2(pyarray_ddma_result_t *output, double *data1, double *data2, int n, int n_angl, int minbox, int maxbox, int integ, int ends, int refresh);

/** @brief Computes 4-th order Directionally dependent DMA for 2D trajectory.
 *
 *  @param *output Pointer to output structure of type pyarray_ddma_result_t
 *  @param *data1 Pointer to X-axis of 2D trajectory
 *  @param *data2 Pointer to Y-axis of 2D trajectory
 *  @param n Length of input vectors data1 and data2
 *  @param n_angl Equally divided angles
 *  @param minbox Smallest box width
 *  @param maxbox Largest box width
 *  @param integ Apply integration
 *  @param ends Use all asymmetric intervals
 *  @param refresh parameter to get rid of the summation error
 *
 *  @return Void
 */
void ddma4(pyarray_ddma_result_t *output, double *data1, double *data2, int n, int n_angl, int minbox, int maxbox, int integ, int ends, int refresh);

/** @brief Function for printing put error message
 *  @param error_text[] error string
 *  @return Void
 */
void error(char error_text[]);

/** Functions to calculate 0-th order DDMA */
double sg0(long m, long n);
double est_f2_ddma0(double *y, long n, long scale, int ends, int i_refresh);
double cmat_ddma0(double k);
int rscale_ddma0(long minbox, long maxbox, double boxratio);

/** Functions to calculate 2-nd order DDMA */
void sg2(double *c0, long m, long n);
double est_f2_ddma2(double *y, long n, long k2, int ends, int i_refresh);
void cmat_ddma2(double k);
int rscale_ddma2(long minbox, long maxbox, double boxratio);

/** Functions to calculate 4-th order DDMA */
double est_f2_ddma4(double *y, long n, long k2, int ends, int i_refresh);
void cmat_ddma4(double k);
int rscale_ddma4(long minbox, long maxbox, double boxratio);

/** Functions to manipulate memory*/
double *vector(long nl, long nh);
int *ivector(long nl, long nh);
long *lvector(long nl, long nh);
void free_vector(double *v, long nl, long nh);
void free_ivector(int *v, long nl, long nh);
void free_lvector(long *v, long nl, long nh);
double **matrix2d(int nrows, int ncolumns);

/** Functions to manipulate memory*/
/** @brief Function for getting C pointer from py array*/
double *pyvector_to_Carrayptrs(PyArrayObject *arrayin);
/** @brief Function for getting C pointer from py array*/
double **ptrvector(long n);
/** @brief Function for converting py matrix to C double pointer matrix*/
double **pymatrix_to_Carrayptrs(PyArrayObject *arrayin);
/** @brief Function for converting C results to Py array result*/
void Pyarray_to_Cstruct_ddma_result(pyarray_ddma_result_t *py_array_input, cstruct_ddma_result_t *c_struct_output);


#endif /*DIRECTED_DMA_H*/