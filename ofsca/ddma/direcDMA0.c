/** @file direcDMA0.c
 *  @brief Directionally dependent DMA analysis of 0-th order
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

#include "ddma.h"

/* Global variables */
long *rs;

void ddma0(pyarray_ddma_result_t *output, double *data1, double *data2, int n, int n_angl, int minbox, int maxbox, int integ, int ends, int refresh)
{
	double *F2,theta,d_theta;
	double *x_d,mu_x;
	long i,j,k;
	clock_t start, end;

    long nr;	/* number of box sizes */
    double *y;

    i_refresh = refresh;

	printf("Total data length = %i\n", n);
	printf("***** DDMA0 *****\n");

	/* Set minimum and maximum box sizes. */
	minbox = (long)(minbox/2)*2+1; /* minbox should be an odd number */
	if(minbox < 5) minbox = 5;
	if(maxbox > n/3 || minbox > maxbox) maxbox = (long)n/3;

	/* "rscale" is based on that of "dfa.c". */
	/* Note that scales are odd numbers in CDMA. */
	nr = rscale_ddma0(minbox, maxbox, pow(2.0, 1.0/8.0));
    /*create C struct for results and assign vector and mtrx shapes*/
    cstruct_ddma_result_t result;
    result.theta_arr_len[0] = n_angl;
    result.scale_arr_len[0] = nr;
    result.F_mtrx_shape[0] = n_angl;
	result.F_mtrx_shape[1] = nr;
	/*link C struct to the PyArray results struct*/
    Pyarray_to_Cstruct_ddma_result(output, &result);
	/* For measurement of processing time */
	start = clock();
	printf("starting time:%ld\n", start);
	
	/* define fluctuation function */
	F2 = vector(1,nr);
	x_d = vector(1,n);
	y = vector(1,n);
	d_theta = 3.141592653589793/(double)n_angl;
	
	for(i=1;i<=n_angl;i++){
		theta = d_theta*((double)i-1.0);

		/*save to results vector*/
		result.theta_v[i-1] = theta;
//		printf("Theta(%ld): %lf\n",i, theta);

		mu_x = 0;
		for(j=1;j<=n;j++){
			x_d[j] = data1[j]*cos(theta) + data2[j]*sin(theta);
			mu_x += x_d[j];
		}
		
		mu_x = mu_x/(double)n;

		if(integ > 0){
		/* integration */
			y[1] = (x_d[1]-mu_x);
			for(k=2;k<=n;k++){
				y[k] = y[k-1]+(x_d[k]-mu_x);
			}
		}else{
			for(k=1;k<=n;k++){
				y[k] = x_d[k]-mu_x;
			}
		}

		/* estimation of squared deviations */
		for(k=1;k<=nr;k++){
			F2[k] = est_f2_ddma0(y, n, rs[k], ends, i_refresh);
			/* output estimated results */
			result.F_mtrx[i-1][k-1] = log(F2[k])/log(10)/2;
			result.scale_v[k-1] = log((double)rs[k]/1.00)/log(10);
//			printf("%ld,%lf,%lf,%lf,%lf\n",i,theta,log((double)rs[k])/log(10),log(F2[k])/log(10)/2,log((double)rs[k]/1.00)/log(10));
		}
	}

	/* For measurement of processing time */
	end = clock();
	printf("Finish time: %ld\n", end);
	printf("Processing time: %ld[ms]\n", (end - start));

	/* Release allocated memory. */
	free_vector(y,1,n);
	free_vector(x_d,1,n);
	free_vector(F2,1,nr);

	return;
}

/* calculation of coefficients */
double cmat_ddma0(double k)
{
	return 1/(2*k+1);
}

double sg0(long m, long n)
{
	return 1/(double)((n-1)/2 + m + 1);
}

double est_f2_ddma0(double *y, long n, long scale, int ends, int i_refresh){
	double f2,a0,a0L,a0R;
	double y00,y10;
	double temp,temp1,temp2,y0L,y0R;
	long i,j; /* n: data length */
	long itemp;
	long k,ic;

	double c1;

	f2 = 0;
	k = (scale-1)/2;
	ic = k+1;
	f2 = 0;

	if(ends==1){
	  /* both ends */
	  y0L=0;
	  y0R=0;
	  for(i=1;i<=k;i++){
	    y0L += y[i];
	    y0R += y[n-i+1];
	  }

	  for(i=1;i<=k;i++){
	    y0L += y[k+i];
	    y0R += y[n-i+1-k];

	    c1 = sg0(i-1 ,scale);
	    a0L = c1*y0L;
	    a0R = c1*y0R;

	    temp1 = (y[i] - a0L);
	    temp2 = (y[n-i+1] - a0R);
	    f2 += temp1*temp1+temp2*temp2;
	  }
	}

	/* initial values */
	y10=0;
	for(i=1;i<=scale;i++){
		y10 += y[i];
	}

	c1 = cmat_ddma0((double)k);
	a0 = c1*y10;

	temp = (y[ic] - a0);
	f2 = temp*temp;

	itemp = n-scale;
	
	for(i=1;i<=itemp;i++){
		if(i % i_refresh == 0){
			y10=0;
			for(j=1;j<=scale;j++){
				y10 += y[i+j];
			}
		}else{
			y00 = y10;
			y10 = y00 + y[i+scale]-y[i];
		}
		
		a0 = c1*y10;

		temp = (y[i+ic] - a0);
		f2 += temp*temp;
	}
	if(ends==1){
	  return f2/(double)(itemp+1);
	}else{
	  return f2/(double)(n);
	}
}

/* This function is based on that of the same names in dfa.c. */
int rscale_ddma0(long minbox, long maxbox, double boxratio)
{
    long ir, n;
    long rw;
    long rslen;	/* length of rs[] */

	/*  */
    rslen = log10(maxbox /minbox) / log10(boxratio) + 1;
    rs = lvector(1, rslen);
    if (!rs) printf("Got NULL!!!\n");
    for (ir = 1, n = 2, rs[1] = minbox; n <= rslen && rs[n-1] < maxbox; ir++)
      if ((rw = minbox * pow(boxratio, ir) + 0.5) > rs[n-1])
            rs[n++] = (long)(rw/2)*2+1;
    if (rs[--n] > maxbox) --n;
    return (n);
}
