/** @file ddma.h
 *  @brief Main file of Directional DMA Python wrapper
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <Python.h>
#include "ddma.h"

#define DEFAULT_REFRESH_VALUE 666

void Pyarray_to_Cstruct_ddma_result(pyarray_ddma_result_t *py_array_input, cstruct_ddma_result_t *c_struct_output)
{
    /*converting Py_array F_mtrx to C pointer type matrix*/
    py_array_input->F_mtrx = PyArray_SimpleNew(2, c_struct_output->F_mtrx_shape, NPY_DOUBLE); //allocating memory for F matrix
    c_struct_output->F_mtrx = pymatrix_to_Carrayptrs(py_array_input->F_mtrx); // converting allocated memory to C ptrs
    /*converting Py_array F_mtrx to C pointer type matrix*/
    py_array_input->theta_v = PyArray_SimpleNew(1, c_struct_output->theta_arr_len, NPY_DOUBLE);
    c_struct_output->theta_v = pyvector_to_Carrayptrs(py_array_input->theta_v); // converting allocated memory to C ptr

    /*converting Py_array F_mtrx to C pointer type matrix*/
    py_array_input->scale_v = PyArray_SimpleNew(1, c_struct_output->scale_arr_len, NPY_DOUBLE);
    c_struct_output->scale_v = pyvector_to_Carrayptrs(py_array_input->scale_v); // converting allocated memory to C ptr
}

static PyObject *get_directedDMA(PyObject *self, PyObject* args, PyObject* kwargs) {
    
    static char* keywords[] = { "data1", 
                                "data2",
                                //optional
                                "n_angles", 
                                "minbox", 
                                "maxbox", 
                                "integrate", 
                                "asym_intervals",
                                "order",
                                "refresh",
                                NULL};

    /** Deafult values*/
    int n_angl = 64;
    int minbox = 7;
    int maxbox = 0;
    int integ = 1;
    int ends = 0; /* using only symmetric intervals */
    int order = 2;
    int refresh = DEFAULT_REFRESH_VALUE;
    
    //input arrays
    PyObject *data1_obj=NULL,*data2_obj=NULL;
    PyArrayObject *vecin, *vecin1;
    double *data1_buf, *data2_buf;
    pyarray_ddma_result_t ddma_result; /*variable for result structure*/

    if (!PyArg_ParseTupleAndKeywords(args, kwargs,
                                     "OO|iiiiiii",
                                     keywords,
                                     &vecin, 
                                     &vecin1,
                                     &n_angl,
                                     &minbox,
                                     &maxbox,
                                     &integ,
                                     &ends,
                                     &order,
                                     &refresh
                                     )) {
        return NULL;
    }

    if (NULL == vecin)  return NULL;
    if (NULL == vecin1)  return NULL;

    data1_obj = PyArray_FROM_OTF(vecin, NPY_DOUBLE, NPY_ARRAY_IN_ARRAY);
    if (data1_obj == NULL) return NULL;
    data2_obj = PyArray_FROM_OTF(vecin1, NPY_DOUBLE, NPY_ARRAY_IN_ARRAY);
    if (data2_obj == NULL) return NULL;

    int n1=PyArray_DIMS(data1_obj)[0];
    int n2=PyArray_DIMS(data2_obj)[0];

    if (n1 != n2) {
        PyErr_SetString(PyExc_ValueError, "n1 != n2 (Input vectors must have equal sizes!)");
        return NULL;
    }

    data1_buf=pyvector_to_Carrayptrs(data1_obj);
    data2_buf=pyvector_to_Carrayptrs(data2_obj);

    /*selecting DDMA order*/
    switch(order) {
        case 0:
            if(refresh == DEFAULT_REFRESH_VALUE) refresh = 10000;
            printf("refresh: %i\n", refresh);
            ddma0(&ddma_result, data1_buf, data2_buf, n1, n_angl, minbox, maxbox, integ, ends, refresh);
            break;
        case 2:
            if(refresh == DEFAULT_REFRESH_VALUE) refresh = 2000;
            printf("refresh: %i\n", refresh);
            ddma2(&ddma_result, data1_buf, data2_buf, n1, n_angl, minbox, maxbox, integ, ends, refresh);
            break;
        case 4:
            if(refresh == DEFAULT_REFRESH_VALUE) refresh = 200;
            printf("refresh: %i\n", refresh);
            ddma4(&ddma_result, data1_buf, data2_buf, n1, n_angl, minbox, maxbox, integ, ends, refresh);
            break;
        default:
            PyErr_SetString(PyExc_ValueError, "order should be 0/2/4 integer value!");
            return NULL;
    }

    PyObject *rslt = PyTuple_New(3);
    PyTuple_SetItem(rslt, 0, ddma_result.theta_v);
    PyTuple_SetItem(rslt, 1, ddma_result.F_mtrx);
    PyTuple_SetItem(rslt, 2, ddma_result.scale_v);

    return rslt;
}

static PyMethodDef methods[] = {
    {
        "get_directedDMA",                              // say hello
        (PyCFunction) get_directedDMA,                  // C wrapper function
        METH_VARARGS | METH_KEYWORDS,                   // received variable args (but really just 1)
        "Computes Directionally Detrended Moving Average."   // documentation
    }, 
    {NULL}
};

static struct PyModuleDef ddma = {
    PyModuleDef_HEAD_INIT,
    "ddma",                                                                 // name of module exposed to Python
    "C extention module that computes Directionally Detrended Moving Average.", // module documentation
    -1,
    methods
};

PyMODINIT_FUNC PyInit_ddma(void) {
    import_array();
    return PyModule_Create(&ddma);
}

#ifdef __cplusplus
}  // extern "C"
#endif