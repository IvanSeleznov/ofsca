/** @file direcDMA4.c
 *  @brief Directionally dependent DMA analysis of 4-th order
 *
 *  @author Ken KIYONO
 *  @author Ivan SELEZNOV
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <time.h>

#include "ddma.h"

/* Global variables */
long *rs;
double c1,c2,c3;
double *loc_sum0,*loc_sum1,*loc_sum2,*loc_sum3,*loc_sum4;


void ddma4(pyarray_ddma_result_t *output, double *data1, double *data2, int n, int n_angl, int minbox, int maxbox, int integ, int ends, int refresh)
{
	double *F2,theta,d_theta;
	double *x_d,mu_x;
	long i,j,k;
	clock_t start, end;
    long nr;	/* number of box sizes */

    double *y;

	i_refresh = refresh;

	printf("Total data length = %i\n", n);
	printf("***** DDMA4 *****\n");

	if(i_refresh > n) i_refresh=n;
	loc_sum0 = vector(1,(long)(n/i_refresh)+1);
	loc_sum1 = vector(1,(long)(n/i_refresh)+1);
	loc_sum2 = vector(1,(long)(n/i_refresh)+1);
	loc_sum3 = vector(1,(long)(n/i_refresh)+1);
	loc_sum4 = vector(1,(long)(n/i_refresh)+1);
		
	/* Set minimum and maximum box sizes. */
	minbox = (long)(minbox/2)*2+1; /* minbox should be an odd number */
	if(minbox < 5) minbox = 5;
	if(maxbox > n/3 || minbox > maxbox) maxbox = (long)n/3;
	
	/* "rscale" is based on that of "dfa.c". */
	/* Note that scales are odd numbers in CDMA. */
	nr = rscale_ddma4(minbox, maxbox, pow(2.0, 1.0/8.0));

    /*create C struct for results and assign vector and mtrx shapes*/
    cstruct_ddma_result_t result;
    result.theta_arr_len[0] = n_angl;
    result.scale_arr_len[0] = nr;
    result.F_mtrx_shape[0] = n_angl;
	result.F_mtrx_shape[1] = nr;
	/*link C struct to the PyArray results struct*/
    Pyarray_to_Cstruct_ddma_result(output, &result);

	/* For measurement of processing time */
	start = clock();
	printf("starting time:%ld\n", start);
	
	/* define fluctuation function */
	F2 = vector(1,nr);	

	x_d = vector(1,n);
	y = vector(1,n);
	d_theta = 3.141592653589793/(double)n_angl;

	for(i=1;i<=n_angl;i++){
		theta = d_theta*((double)i-1.0);
		/*save to results vector*/
		result.theta_v[i-1] = theta;
//		printf("Theta(%ld): %lf\n",i, theta);
		mu_x = 0;
		for(j=1;j<=n;j++){
			x_d[j] = data1[j]*cos(theta) + data2[j]*sin(theta);
			mu_x += x_d[j];
		}
		mu_x = mu_x/(double)n;
		if(integ > 0){
		/* integration */
			y[1] = (x_d[1]-mu_x);
			for(k=2;k<=n;k++){
				y[k] = y[k-1]+(x_d[k]-mu_x);
			}
		}else{
			for(k=1;k<=n;k++){
				y[k] = x_d[k]-mu_x;
			}
		}
		/* estimation of squared deviations */
		for(k=1;k<=nr;k++){
			F2[k] = est_f2_ddma4(y, n, k, ends, i_refresh);
			/* output estimated results */
			/* output estimated results */
			result.F_mtrx[i-1][k-1] = log(F2[k])/log(10)/2;
			result.scale_v[k-1] = log((double)rs[k]/2.74)/log(10);
//			printf("%ld,%lf,%lf,%lf,%lf\n",i,theta,log((double)rs[k])/log(10),log(F2[k])/log(10)/2,log((double)rs[k]/2.74)/log(10));
		}
	}

	end = clock();
	printf("Finish time: %ld\n", end);
	printf("Processing time: %ld[ms]\n", (end - start));

	/* Release allocated memory. */
	free_vector(y,1,n);
	free_vector(x_d,1,n);
	free_vector(F2,1,nr);
	free_vector(loc_sum0,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum1,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum2,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum3,1,(long)(n/i_refresh)+1);
	free_vector(loc_sum4,1,(long)(n/i_refresh)+1);
	
	return;
}

/* calculation of coefficients */
void cmat_ddma4(double k)
{
	double k2,k3,k4,den;

	k2 = k*k;
	k3 = k2*k;
	k4 = k3*k;
	den = 180 + 72*k - 800*k2 - 320*k3 + 320*k4 + 128* k*k4;
	c1 = (180 - 750*k - 525*k2 + 450*k3 + 225*k4) / den;
	c2 = (1575 - 1050*k - 1050*k2) / den;
    c3 = 945/ den;
}

double est_f2_ddma4(double *y, long n, long k2, int ends, int i_refresh)
{
	double f2,a0;
	double y00,y0i,y0ii,y0iii,y0iv,y10,y1i,y1ii,y1iii,y1iv;
	double temp1,temp2;
	double nn1,nn2,nn3,nn4;
	long i,j,i0; /* n: data length */
	long itemp;
	long k,kb,ic,scale,iloc;
	
	scale = rs[k2];
	k = (scale-1)/2;
	ic = k+1;

	/* initial values */
	iloc=1;
	kb = (rs[k2-1]-1)/2;
	nn1=(double)(kb-k);
	nn2=nn1*nn1;
	nn3=nn2*nn1;
	nn4=nn2*nn2;
	
	if(k2 == 1){
		i0 = 1;
		y10=0;
		y1i=0;
		y1ii=0;
		y1iii=0;
		y1iv=0;
	}else{
		i0 = rs[k2-1]+1;
		y10=loc_sum0[iloc];
		y1i=loc_sum1[iloc]+loc_sum0[iloc]*nn1;
		y1ii=loc_sum2[iloc]+2*loc_sum1[iloc]*nn1+loc_sum0[iloc]*nn2;
		y1iii=loc_sum3[iloc]+3*loc_sum2[iloc]*nn1+3*loc_sum1[iloc]*nn2+loc_sum0[iloc]*nn3;
		y1iv=loc_sum4[iloc]+4*loc_sum3[iloc]*nn1+6*loc_sum2[iloc]*nn2+4*loc_sum1[iloc]*nn3+loc_sum0[iloc]*nn4;
	}

	for(i=i0;i<=scale;i++){
		y10 += y[i];
		temp1 = y[i]*(double)(i-k-1);
		y1i += temp1;
		temp2 = temp1*(double)(i-k-1);
		y1ii += temp2;
		temp1 = temp2*(double)(i-k-1);
		y1iii += temp1;
		temp2 = temp1*(double)(i-k-1);
		y1iv += temp2;
	}
	
	loc_sum0[iloc]=y10;
	loc_sum1[iloc]=y1i;
	loc_sum2[iloc]=y1ii;
	loc_sum3[iloc]=y1iii;
	loc_sum4[iloc]=y1iv;
	
	
	cmat_ddma4((double)k);
	
	a0 = c1*y10+c2*y1ii+c3*y1iv;
	temp1 = (y[ic] - a0);
	f2 = temp1*temp1;

	itemp = n-scale;

	for(i=1;i<=itemp;i++){
		if(i % i_refresh == 0){
			iloc++;
			if(k2 == 1){
				i0 = 1;
				y10=0;
				y1i=0;
				y1ii=0;
				y1iii=0;
				y1iv=0;
			}else{
				i0 = rs[k2-1]+1;
				y10=loc_sum0[iloc];
				y1i=loc_sum1[iloc]+loc_sum0[iloc]*nn1;
				y1ii=loc_sum2[iloc]+2*loc_sum1[iloc]*nn1+loc_sum0[iloc]*nn2;
				y1iii=loc_sum3[iloc]+3*loc_sum2[iloc]*nn1+3*loc_sum1[iloc]*nn2+loc_sum0[iloc]*nn3;
				y1iv=loc_sum4[iloc]+4*loc_sum3[iloc]*nn1+6*loc_sum2[iloc]*nn2+4*loc_sum1[iloc]*nn3+loc_sum0[iloc]*nn4;
			}
			
			for(j=i0;j<=scale;j++){
				y10 += y[i+j];
				temp1 = y[i+j]*(double)(j-k-1);
				y1i += temp1;
				temp2 = temp1*(double)(j-k-1);
				y1ii += temp2;
				temp1 = temp2*(double)(j-k-1);
				y1iii += temp1;
				temp2 = temp1*(double)(j-k-1);
				y1iv += temp2;
			}
			loc_sum0[iloc]=y10;
			loc_sum1[iloc]=y1i;
			loc_sum2[iloc]=y1ii;
			loc_sum3[iloc]=y1iii;
			loc_sum4[iloc]=y1iv;
		}else{
			y00 = y10;
			y0i = y1i;
			y0ii = y1ii;
			y0iii = y1iii;
			y0iv = y1iv;

			y10 = y00 + y[i+scale]-y[i];
			temp1 = y[i]*(double)(k+1);
			temp2 = y[i+scale]*(double)k;
			y1i = y0i - y00 + temp1 + temp2;
			temp1 = temp1*(double)(k+1);
			temp2 = temp2*(double)k;
			y1ii = y0ii - 2*y0i + y00 - temp1 + temp2;
			temp1 = temp1*(double)(k+1);
			temp2 = temp2*(double)k;
			y1iii = y0iii - 3*y0ii + 3*y0i - y00 + temp1 + temp2;
			y1iv = y0iv - 4*y0iii + 6* y0ii - 4 * y0i + y00 - temp1 *(double)(k+1) + temp2*(double)k;
		}
		
		a0 = c1*y10+c2*y1ii+c3*y1iv;

		temp1 = (y[i+ic] - a0);
		f2 += temp1*temp1;
	}

	return f2/(double)(itemp+1);
}

/* This function is based on that of the same names in dfa.c. */
int rscale_ddma4(long minbox, long maxbox, double boxratio)
{
    long ir, n;
    long rw;
    long rslen;	/* length of rs[] */

	/*  */
    rslen = log10(maxbox /minbox) / log10(boxratio) + 1;
    rs = lvector(1, rslen);
    for (ir = 1, n = 2, rs[1] = minbox; n <= rslen && rs[n-1] < maxbox; ir++)
      if ((rw = minbox * pow(boxratio, ir) + 0.5) > rs[n-1])
            rs[n++] = (long)(rw/2)*2+1;
    if (rs[--n] > maxbox) --n;
    return (n);
}
