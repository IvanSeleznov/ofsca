# import wii_board_data_reader as wii
import os
from .. import ofsca
import matplotlib.pyplot as plt

example_file_path = os.path.abspath(os.path.dirname(__file__))

import numpy as np

class WiiBoardData:
    def __init__(self, file_path=None):
        """
        Class for reading and resampling Wii Board data.
        Wii board platfrom sampling frequency is not fixed, so interpolation is needed to achieve a stable sampling frequency
        :param file_path: [str] path to Wii Board csv file
        """
        self.time_ms = []
        self.top_left_f_kg = []
        self.top_right_f_kg = []
        self.bottom_left_f_kg = []
        self.bottom_right_f_kg = []
        self.cop_x = []
        self.cop_y = []
        self.total_f = []
        self.resampled_f_hz = 0
        self.resampled_time_ms = []
        self.resampled_cop_x = []
        self.resampled_cop_y = []

        self.read_wii_board_data(file_path)

    def __norm_and_resample_time_vect(self, f_hz, original_time_vect):
        time_vect_norm = (original_time_vect - original_time_vect[0])/1000
        T = time_vect_norm[-1]
        F = f_hz
        dt = 1/F
        time_vect_resampled = np.arange(0, T + dt, dt)
        return time_vect_resampled


    def __resample_data(self, data, time_vect):
        import scipy.signal as sc
        resampled_data, _ = sc.resample(data, len(time_vect), time_vect)
        return resampled_data

    def __remove_spikes_from(self, vector):
        diff = np.diff(vector)
        std = np.std(diff)
        median = np.median(diff)
        centered = diff - median
        spikes = np.array(np.where(np.abs(centered) > 15*std)) + 1
        print(spikes)

        vector[spikes] = np.median(vector)


    def read_wii_board_data(self, file_path):
        import pandas as pd
        cvs_data = pd.read_csv(file_path, ' ')

        # wii_data = WiiBoardData()
        self.time_ms = cvs_data.iloc[:, 0]
        self.top_left_f_kg = cvs_data.iloc[:, 1]
        self.top_right_f_kg = cvs_data.iloc[:, 2]
        self.bottom_left_f_kg = cvs_data.iloc[:, 3]
        self.bottom_right_f_kg = cvs_data.iloc[:, 4]
        self.cop_x = np.array(cvs_data.iloc[:, 5])
        self.cop_y = np.array(cvs_data.iloc[:, 6] * -1)
        self.total_f = cvs_data.iloc[:, 7]

        f_hz = 100
        time_vect_resampled = self.__norm_and_resample_time_vect(f_hz, original_time_vect=np.array(self.time_ms))
        self.resampled_f_hz = f_hz
        self.resampled_time_ms = time_vect_resampled
        self.resampled_cop_x = self.__resample_data(np.array(self.cop_x), time_vect_resampled)
        self.resampled_cop_y = self.__resample_data(np.array(self.cop_y), time_vect_resampled)


def example_backward_leaning():
    """
    Example demonstrating OFSCA analysis for CoP (Center-of-Pressure) trajectory
    Subject performing quite standing with eyes opened.
    :return:
            fig: [matplotlib figure obj]
    """
    example1_filename = 'data/cop/backward-right.csv'
    path = os.path.join(example_file_path, example1_filename)
    data = WiiBoardData(path)

    ofsca_obj = ofsca.OFSCA(x_vector=data.cop_x,
                            y_vector=data.cop_y,
                            order=4,
                            start_scale=1.2,
                            end_scale=2.5,
                            make_integration=True,
                            n_angles=64)
    ofsca_obj.perform_OFSCA()

    fig = ofsca_obj.get_ofsca_fig(rcParams={'font.size': 14}, alpha_graphs_ylim=[0.4, 2.8])
    return fig

def example_standing():
    """
    Example demonstrating OFSCA analysis for CoP (Center-of-Pressure) trajectory
    Subject performing leaning backward oscillations.
    :return:
            fig: [matplotlib figure obj]
    """
    example2_filename = 'data/cop/base.csv'
    path = os.path.join(example_file_path, example2_filename)
    data = WiiBoardData(path)

    ofsca_obj = ofsca.OFSCA(x_vector=data.cop_x,
                            y_vector=data.cop_y,
                            order=4,
                            start_scale=1.2,
                            end_scale=2.5,
                            make_integration=True,
                            n_angles=64)
    ofsca_obj.perform_OFSCA()

    fig = ofsca_obj.get_ofsca_fig(rcParams={'font.size': 14}, alpha_graphs_ylim=[0.4, 2.8])
    return fig
