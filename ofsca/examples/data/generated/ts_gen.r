### Change n, H1, H2 and deg
##########################################
### If you don't have "longmemo" package, install it.
# install.packages("longmemo")
require(longmemo)
#
DIR <- "/"
# data length
n <- 10000
#####################
# rotation angle: 0 - 180 deg
deg <- 30 # 45
#####################
# fGn parameters
H1 <- 0.8
H2 <- 0.6
#########################
x <- simFGN0(n,H1)
y <- simFGN0(n,H2)
#########################
x.r <- cos(pi*deg/180)*x - sin(pi*deg/180)*y
y.r <- sin(pi*deg/180)*x + cos(pi*deg/180)*y
x <- x.r
y <- y.r
#########################
par(pty="s")
r.max <- max(abs(x),abs(y))
plot(x,y,type="l",col=2,xlim=c(-r.max,r.max),ylim=c(-r.max,r.max))
#########################
setwd(DIR)
write.table(data.frame(x,y),"samp_30_86.csv", sep = ",", append=FALSE, quote=FALSE, col.names=FALSE,row.names=FALSE)

