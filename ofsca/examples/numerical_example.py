import os
from .. import ofsca
import matplotlib.pyplot as plt
import numpy as np

example_file_path = os.path.abspath(os.path.dirname(__file__))

def get_data(path=None):
    """
    Function for reading CSV file with generated data.
    :param path: path to the file
    :return:
            NumData class object
    """
    class NumData():
        def __init__(self):
            self.x = []
            self.y = []

    Ndata = NumData()

    import pandas as pd
    data = pd.read_csv(path, ',')
    Ndata.x = np.array(data.iloc[:, 0])
    Ndata.y = np.array(data.iloc[:, 1])

    return Ndata

def example_numerical():
    """
    Example demonstrating OFSCA analysis for numerically generated time series.
    2D trajectory consists of two 1D fractional Gaussian noise with Hurst exponets H1 = 0.8 and H2 = 0.6 respectively
    Then two time series are rotated by 80 and 30 degrees respectively.

    R script was used to generate the data. Please see data/generated/ts_gen.r script
    :return:
            fig: [matplotlib figure obj]
    """
    example1_filename = 'data/generated/samp_30_80_86.csv'
    path = os.path.join(example_file_path, example1_filename)

    data = get_data(path)

    ofsca_obj = ofsca.OFSCA(x_vector=data.x,
                            y_vector=data.y,
                            order=4,
                            start_scale=1.2,
                            end_scale=3.5,
                            make_integration=True,
                            n_angles=64)
    ofsca_obj.perform_OFSCA()

    fig = ofsca_obj.get_ofsca_fig(rcParams={'font.size': 14}, alpha_graphs_ylim=[0.4, 1.0])
    return fig