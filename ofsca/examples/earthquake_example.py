import os
import matplotlib.pyplot as plt
import numpy as np
from .. import ofsca
import pandas as pd


def read_earthquake_data(file_path):
    class EarthquakeData:
        def __init__(self):
            self.NS = []
            self.EW = []
            self.UD = []
            self.site_code = 0
            self.lon = 0
            self.lat = 0
            self.fs = 100  # Hz

    EQData = EarthquakeData()

    def parse_header(file_path):
        with open(file_path) as fp:
            line = fp.readline()
            cnt = 1
            while cnt < 8:
                # if cnt == 1:
                #     EQData.site_code = int(line.split("=")[1].rsplit(" ")[1])
                # elif cnt == 2:
                if cnt == 2:
                    EQData.lat = float(line.split("=")[1])
                elif cnt == 3:
                    EQData.lon = float(line.split("=")[1])
                elif cnt == 4:
                    EQData.fs = float(line.split("=")[1].rsplit("Hz")[0])
                else:
                    print("Line {}: {}".format(cnt, line.strip()))

                line = fp.readline()
                cnt += 1

    parse_header(file_path)
    data = pd.read_csv(file_path, ',', header=7)

    EQData.NS = data.iloc[:, 0]
    EQData.EW = data.iloc[:, 1]
    EQData.UD = data.iloc[:, 2]

    return EQData


def earthquake_example():

    earthquake_data_filename = 'data/earthquake/T_Wakuya_Shinmachi.txt'
    path = os.path.join(os.path.abspath(os.path.dirname(__file__)), earthquake_data_filename)

    data = read_earthquake_data(path)

    data.NS = data.NS[1000:20000]
    data.EW = data.EW[1000:20000]

    titles = ["(e)", "(f)", "(g)", "(h)", "(i)", "(j)", "(k)", "(l)", "(m)", "(n)", "(o)", "(p)", "(q)", "(r)", "(s)",
              "(t)", "(u)", "(v)", "(w)", "(x)", "(y)", "(z)"]

    ranges_to_cut = [
        (0, 3200),      #P-wave
        (3200, 8000),   #1st S-wave
        (8000, 13000),  #2nd S-wave
        (13000, 19000)  #aftershock wave
    ]

    start_scale = 1.3
    end_scale = 2.2
    order = 4
    titles_fontsize = 12

    min_logF_comp = 0
    max_logF_comp = 0
    min_slope_comp = 0
    max_slope_comp = 0
    min_logF_surf = 0
    max_logF_surf = 0
    min_slope_surf = 0
    max_slope_surf = 0

    ofsca_results_dict = {}
    for i, ranges in enumerate(ranges_to_cut):
        #perform OFSCA calculation
        earthquake_ofsca_obj = ofsca.OFSCA(x_vector=np.array(data.NS[ranges[0]:ranges[1]]),
                                           y_vector=np.array(data.EW[ranges[0]:ranges[1]]),
                                           order=order,
                                           start_scale=start_scale,
                                           end_scale=end_scale,
                                           make_integration=True,
                                           n_angles=64)
        earthquake_ofsca_obj.perform_OFSCA()

        #get slopes
        slope_comp1 = earthquake_ofsca_obj.get_local_slopes(log_s=earthquake_ofsca_obj.comp1.log_s, log_F=earthquake_ofsca_obj.comp1.log_F, points_for_estimation=20)
        slope_comp2 = earthquake_ofsca_obj.get_local_slopes(log_s=earthquake_ofsca_obj.comp2.log_s, log_F=earthquake_ofsca_obj.comp2.log_F, points_for_estimation=20)


        if i > 0:
            min_logF_comp = min(earthquake_ofsca_obj.comp1.log_F.min(), earthquake_ofsca_obj.comp2.log_F.min()) if min(earthquake_ofsca_obj.comp1.log_F.min(), earthquake_ofsca_obj.comp2.log_F.min()) < min_logF_comp else min_logF_comp
            max_logF_comp = max(earthquake_ofsca_obj.comp1.log_F.max(), earthquake_ofsca_obj.comp2.log_F.max()) if max(earthquake_ofsca_obj.comp1.log_F.max(), earthquake_ofsca_obj.comp2.log_F.max()) > max_logF_comp else max_logF_comp
            min_slope_comp = min(slope_comp1.min(), slope_comp2.min()) if min(slope_comp1.min(), slope_comp2.min()) < min_slope_comp else min_slope_comp
            max_slope_comp = max(slope_comp1.max(), slope_comp2.max()) if max(slope_comp1.max(), slope_comp2.max()) > max_slope_comp else max_slope_comp
            min_logF_surf = earthquake_ofsca_obj.log_F_matrix.min() if earthquake_ofsca_obj.log_F_matrix.min() < min_logF_surf else min_logF_surf
            max_logF_surf = earthquake_ofsca_obj.log_F_matrix.max() if earthquake_ofsca_obj.log_F_matrix.max() > max_logF_surf else max_logF_surf
            min_slope_surf = earthquake_ofsca_obj.local_slopes[:, 5:-5].min() if earthquake_ofsca_obj.local_slopes[:, 5:-5].min() < min_slope_surf else min_slope_surf
            max_slope_surf = earthquake_ofsca_obj.local_slopes[:, 5:-5].max() if earthquake_ofsca_obj.local_slopes[:, 5:-5].max() > max_slope_surf else max_slope_surf
        else:
            min_logF_comp = min(earthquake_ofsca_obj.comp1.log_F.min(), earthquake_ofsca_obj.comp2.log_F.min())
            max_logF_comp = max(earthquake_ofsca_obj.comp1.log_F.max(), earthquake_ofsca_obj.comp2.log_F.max())
            min_slope_comp = min(slope_comp1.min(), slope_comp2.min())
            max_slope_comp = max(slope_comp1.max(), slope_comp2.max())
            min_logF_surf = earthquake_ofsca_obj.log_F_matrix.min()
            max_logF_surf = earthquake_ofsca_obj.log_F_matrix.max()
            min_slope_surf = earthquake_ofsca_obj.local_slopes[:, 5:-5].min()
            max_slope_surf = earthquake_ofsca_obj.local_slopes[:, 5:-5].max()

        ofsca_results_dict.update({
            i : {
                "log_F_m" : earthquake_ofsca_obj.log_F_matrix,
                "log_n_m" : earthquake_ofsca_obj.log_s_matrix,
                "theta_v" : earthquake_ofsca_obj.theta_vector,
                "slope_theta": earthquake_ofsca_obj.slope_theta,
                "slope_m" : earthquake_ofsca_obj.local_slopes,
                "theta1" : earthquake_ofsca_obj.comp1.theta,
                "theta2": earthquake_ofsca_obj.comp2.theta,
                "theta1_hat": earthquake_ofsca_obj.comp1.theta_hat,
                "theta2_hat": earthquake_ofsca_obj.comp2.theta_hat,
                "comp1":  {
                    "v" : earthquake_ofsca_obj.comp1.ts,
                    "log_n" : earthquake_ofsca_obj.comp1.log_s,
                    "log_F": earthquake_ofsca_obj.comp1.log_F,
                    "slope": slope_comp1,
                },
                "comp2":  {
                    "v" : earthquake_ofsca_obj.comp2.ts,
                    "log_n" : earthquake_ofsca_obj.comp2.log_s,
                    "log_F": earthquake_ofsca_obj.comp2.log_F,
                    "slope": slope_comp2
                }
            }
        })


    fig = plt.figure()
    fig.set_size_inches(16, 24, forward=True)
    from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec

    plt.rcParams.update({'font.size': 12})
    gs = GridSpec(nrows=6, ncols=len(ranges_to_cut), hspace=0.8, wspace=0.3)

    gs00 = GridSpecFromSubplotSpec(2, 3, subplot_spec=gs[0, :], wspace=0.3, hspace=0.15)
    ax11 = fig.add_subplot(gs00[0, 0:3])
    ax12 = fig.add_subplot(gs00[1, 0:3])

    ax11.plot(np.arange(0, data.NS.shape[0] / data.fs, 1 / data.fs), data.NS)
    ax11.grid(b=True)
    ax11.set_xlim((0, data.NS.shape[0] / data.fs))
    ax11.get_xaxis().set_visible(False)
    plt.text(0.07, 1.2, '(a)', horizontalalignment='center', verticalalignment='center', transform=ax11.transAxes,
             fontsize=titles_fontsize)
    plt.text(0.27, 1.2, '(b)', horizontalalignment='center', verticalalignment='center', transform=ax11.transAxes,
             fontsize=titles_fontsize)
    plt.text(0.52, 1.2, '(c)', horizontalalignment='center', verticalalignment='center', transform=ax11.transAxes,
             fontsize=titles_fontsize)
    plt.text(0.8, 1.2, '(d)', horizontalalignment='center', verticalalignment='center', transform=ax11.transAxes,
             fontsize=titles_fontsize)

    ax11.axvline(x=ranges_to_cut[2][1], linewidth=2, color='r', linestyle="--")
    ax12.axvline(x=ranges_to_cut[2][1], linewidth=2, color='r', linestyle="--")

    ax12.plot(np.arange(0, data.EW.shape[0] / data.fs, 1 / data.fs), data.EW)
    ax12.grid(b=True)
    ax12.set_xlim((0, data.NS.shape[0] / data.fs))
    ax12.set_xlabel("t, sec")
    ax12.set_ylabel("ampl, gal(cm/s/s)")

    c = 0
    for i, ranges in enumerate(ranges_to_cut):

        ax11.axvline(x=ranges_to_cut[i][0] / data.fs, linewidth=2, color='r', linestyle="--")
        ax12.axvline(x=ranges_to_cut[i][0] / data.fs, linewidth=2, color='r', linestyle="--")

        '''----------------------------------------------------------------------------------------------------------
                                                    Log F function surface
        -------------------------------------------------------------------------------------------------------------'''
        ax1 = fig.add_subplot(gs[1, i], projection="polar")
        earthquake_ofsca_obj.visualize_fluctuation_function_surface(log_F_mtrx=ofsca_results_dict[i].get("log_F_m"),    #earthquake_ofsca_obj.log_F_matrix,
                                                                    log_s_matrix=ofsca_results_dict[i].get("log_n_m"),  #earthquake_ofsca_obj.log_n_matrix,
                                                                    theta_v=ofsca_results_dict[i].get("theta_v"),       #earthquake_ofsca_obj.theta_vector,
                                                                    ax=ax1,
                                                                    title=titles[c],
                                                                    levels=10,
                                                                    normalize=[min_logF_surf, max_logF_surf],
                                                                    angles_labels_size=16,
                                                                    angles_pad=3,
                                                                    title_fontsize=titles_fontsize,
                                                                    colorbar_pad=0.2,
                                                                    colorbar_shrink=0.8)

        earthquake_ofsca_obj.highlight_angles_on_polar_plot(axes=ax1,
                                                            angles_to_highlight=[ofsca_results_dict[i].get("theta1"),  #earthquake_ofsca_obj.theta1,
                                                                                 ofsca_results_dict[i].get("theta2")], #earthquake_ofsca_obj.theta2],
                                                            colors=["r", "b"])
        c += 1

        '''----------------------------------------------------------------------------------------------------------
                                                Local Slope function surface
        -------------------------------------------------------------------------------------------------------------'''
        ax2 = fig.add_subplot(gs[2, i], projection="polar")
        earthquake_ofsca_obj.visualize_local_slopes_surface(local_slopes_mtrx=ofsca_results_dict[i].get("slope_m")[:, 5:-5],   #earthquake_ofsca_obj.local_slopes[:, 5:-5],
                                                            log_s_matrix=ofsca_results_dict[i].get("log_n_m")[:, 5:-5],
                                                            theta_v=ofsca_results_dict[i].get("theta_v"),
                                                            ax=ax2,
                                                            levels=10,
                                                            normalize=[min_slope_surf, max_slope_surf],
                                                            title=titles[c],
                                                            angles_labels_size=16,
                                                            angles_pad=3,
                                                            title_fontsize=titles_fontsize,
                                                            colorbar_pad=0.2,
                                                            colorbar_shrink=0.8)

        earthquake_ofsca_obj.highlight_angles_on_polar_plot(axes=ax2,
                                                            angles_to_highlight=[ofsca_results_dict[i].get("theta1"),
                                                                                 ofsca_results_dict[i].get("theta2")],
                                                            colors=["r", "b"])
        c += 1

        '''----------------------------------------------------------------------------------------------------------
                                                Independent components DMA results
        -------------------------------------------------------------------------------------------------------------'''
        import matplotlib
        cmap = matplotlib.cm.get_cmap('coolwarm')

        ax3 = fig.add_subplot(gs[3, i])
        str_idx, _ = earthquake_ofsca_obj.get_the_closest_value(list=ofsca_results_dict[i].get("comp2")["log_n"],
                                                                value=start_scale)
        ed_idx, _ = earthquake_ofsca_obj.get_the_closest_value(list=ofsca_results_dict[i].get("comp2")["log_n"],
                                                               value=end_scale)

        ax3.plot(ofsca_results_dict[i].get("comp1")["log_n"], ofsca_results_dict[i].get("comp1")["log_F"],
                 color=cmap(1.0), linestyle='--', linewidth=1, marker='o',
                 label=fr"$\epsilon_1, \theta_1 = {np.degrees(ofsca_results_dict[i].get('theta1')):0.0f}^\circ$")
        ax3.plot(ofsca_results_dict[i].get("comp2")["log_n"], ofsca_results_dict[i].get("comp2")["log_F"],
                 color=cmap(0.0), linestyle='--', linewidth=1, marker='o',
                 label=fr"$\epsilon_2, \theta_2 = {np.degrees(ofsca_results_dict[i].get('theta2')):0.0f}^\circ$")

        ax3.vlines(ofsca_results_dict[i].get("comp1")["log_n"][str_idx], np.min(ofsca_results_dict[i].get("comp1")["log_F"]), np.max(ofsca_results_dict[i].get("comp1")["log_F"]),
                   color='gray', linewidth=2, linestyle='--')
        ax3.vlines(ofsca_results_dict[i].get("comp2")["log_n"][ed_idx], np.min(ofsca_results_dict[i].get("comp1")["log_F"]), np.max(ofsca_results_dict[i].get("comp1")["log_F"]),
                   color='gray', linewidth=2, linestyle='--')
        ax3.set_title(titles[c], fontsize=titles_fontsize)
        ax3.grid(b=True)
        ax3.set_xlabel(r"$log_{10} s$")
        ax3.set_ylabel(r"$log_{10} F^(\theta)$")
        ax3.set_frame_on(True)
        ax3.legend(fontsize=10)
        ax3.set_ylim(min_logF_comp-0.1, max_logF_comp+0.1)
        c += 1

        '''----------------------------------------------------------------------------------------------------------
                                            local slopes of log_F of independent components
        -------------------------------------------------------------------------------------------------------------'''
        ax4 = fig.add_subplot(gs[4, i])
        # slope_comp1 = earthquake_ofsca_obj.get_local_slopes(log_n=logn_comp1, log_F=logF_comp1, points_for_estimation=20)
        ax4.plot(ofsca_results_dict[i].get("comp1")["log_n"][5:-5], ofsca_results_dict[i].get("comp1")["slope"][5:-5],
                 color=cmap(1.0), linestyle='--', linewidth=1, marker='o',
                 label=fr"$\epsilon_1, \theta_1 = {np.degrees(ofsca_results_dict[i].get('theta1')):0.0f}^\circ$")
        # slope_comp2 = earthquake_ofsca_obj.get_local_slopes(log_n=logn_comp2, log_F=logF_comp2, points_for_estimation=20)
        ax4.plot(ofsca_results_dict[i].get("comp2")["log_n"][5:-5], ofsca_results_dict[i].get("comp2")["slope"][5:-5],
                 color=cmap(0.0), linestyle='--', linewidth=1, marker='o',
                 label=fr"$\epsilon_2, \theta_2 = {np.degrees(ofsca_results_dict[i].get('theta2')):0.0f}^\circ$")

        ax4.vlines(ofsca_results_dict[i].get("comp2")["log_n"][str_idx],
                   np.min(ofsca_results_dict[i].get("comp1")["slope"][5:-5]),
                   np.max(ofsca_results_dict[i].get("comp1")["slope"][5:-5]), color='gray', linewidth=2, linestyle='--')
        ax4.vlines(ofsca_results_dict[i].get("comp2")["log_n"][ed_idx],
                   np.min(ofsca_results_dict[i].get("comp2")["slope"][5:-5]),
                   np.max(ofsca_results_dict[i].get("comp2")["slope"][5:-5]), color='gray', linewidth=2, linestyle='--')

        # ax6.set_xlim(np.min(logn_comp1), np.max(logn_comp1))
        ax4.set_title(titles[c], fontsize=titles_fontsize)
        ax4.set_xlabel(f"$log$ $s$")
        ax4.set_ylabel(r"$ \alpha^{(\theta)}(s)$")
        ax4.grid(b=True)
        ax4.set_frame_on(True)
        ax4.legend(loc="lower right", fontsize=10)
        ax4.set_ylim(min_slope_comp-0.1, max_slope_comp+0.1)
        c += 1

        '''----------------------------------------------------------------------------------------------------------
                                                    slope theta dependency
        -------------------------------------------------------------------------------------------------------------'''
        ax5 = fig.add_subplot(gs[5, i])
        ax5.plot(ofsca_results_dict[i].get("theta_v"), ofsca_results_dict[i].get("slope_theta"), linewidth=2)

        theta1_hat_idx = np.where(ofsca_results_dict[i].get("theta_v") == ofsca_results_dict[i].get("theta1_hat"))[0][0]
        theta2_hat_idx = np.where(ofsca_results_dict[i].get("theta_v") == ofsca_results_dict[i].get("theta2_hat"))[0][0]
        min_slope_value = np.min(ofsca_results_dict[i].get("slope_theta"))
        max_slope_value = np.max(ofsca_results_dict[i].get("slope_theta"))

        ax5.plot(ofsca_results_dict[i].get("theta_v")[theta1_hat_idx], min_slope_value, "ro",
                 label=fr"$\^\theta_1$ = {np.degrees(ofsca_results_dict[i].get('theta_v')[theta1_hat_idx]):0.0f}$^\circ, \alpha({np.degrees(ofsca_results_dict[i].get('theta_v')[theta1_hat_idx]):0.0f}^\circ) = {min_slope_value:0.2f}$")

        ax5.plot(ofsca_results_dict[i].get("theta_v")[theta2_hat_idx], max_slope_value, "bo",
                 label=fr"$\^\theta_2$ = {np.degrees(ofsca_results_dict[i].get('theta_v')[theta2_hat_idx]):0.0f}$^\circ, \alpha({np.degrees(ofsca_results_dict[i].get('theta_v')[theta2_hat_idx]):0.0f}^\circ) = {max_slope_value:0.2f}$")
        ax5.axvline(x=ofsca_results_dict[i].get("theta_v")[theta1_hat_idx], color='r', linestyle='--', linewidth=2)
        ax5.axvline(x=ofsca_results_dict[i].get("theta_v")[theta2_hat_idx], color='b', linestyle='--', linewidth=2)
        #### slopes ranges
        ax5.hlines(0.5, 0, np.pi, color='gray', linewidth=1.5)
        ax5.hlines(1, 0, np.pi, color='gray', linewidth=1.5)
        ax5.hlines(1.5, 0, np.pi, color='gray', linewidth=1.5)
        ####
        ax5.set_ylim(0.0, 2.0)
        ax5.set_title(titles[c], fontsize=titles_fontsize)
        ax5.legend(fontsize=10)
        ax5.grid(b=True)
        ax5.set_xlabel(r"$\theta, rad$")
        ax5.set_ylabel(r"slope")
        c += 1

        import scipy.stats
        r, p_value = scipy.stats.pearsonr(ofsca_results_dict[i].get("comp1")["v"], ofsca_results_dict[i].get("comp2")["v"])
        print(f"Pearson correlation between components: R, P_value: {r}, {p_value}")

    return fig

