import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib as mpl
from sklearn.linear_model import LinearRegression
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
from ofsca.ddma import get_directedDMA
from ofsca.cdma import get_cDMA
import scipy.stats

class LocalRegression:
    def __init__(self, log_s=None, log_F=None, points_to_estim=10):
        """
        Helper class for calculating local regression

        :param log_s: [1-D numpy array] vector of log scale
        :param log_F: [1-D numpy array] vector of log fluctuation function
        :param points_to_estim: [int] points for estimating the local slopes, default: 10
        """
        self.points_to_estim = points_to_estim
        self.log_s = log_s
        self.log_F = log_F
        self.local_regression = self.__get_local_regression_lines(self.points_to_estim)
        self.local_slopes = self.get_local_slopes()

    def __get_local_regression_lines(self, points=None):
        """
        Main method for fitting regression lines to windowed time series to obtain local slopes

        :param points: [int] points for estimating the local slopes
        :return:
                predictions: [dict] dictionary that holds the result
                            {
                                "y_preds" : [tuple] (trimmed(windowed)local log n vector, local regression line predictions)
                                "regressor": [LinearRegression obj]
                                "point": [tuple] (index of the current point, the point itself)
                            }
        """
        from sklearn.linear_model import LinearRegression
        win_len = int(np.floor(points / 2) * 2 + 1)
        diff_points = int(np.floor(win_len / 2))
        N = len(self.log_F)

        predictions = []
        # print(f"diff_points: {diff_points}")
        for i, point in enumerate(self.log_F):

            # start_point, end_point = 0, 0
            if i < diff_points:
                start_point = 0
                end_point = i + diff_points
            elif i >= diff_points and i < N - diff_points:  # other points
                start_point = i - diff_points
                end_point = i + diff_points
            elif i >= N - diff_points:
                start_point = i - diff_points
                end_point = N - 1
            else:
                ValueError("erorr")  # TODO: remove it

            temp_log_s = self.log_s[start_point:end_point].reshape(-1, 1)
            temp_log_F = self.log_F[start_point:end_point].reshape(-1, 1)

            regressor = LinearRegression()
            regressor.fit(temp_log_s, temp_log_F)  # training the algorithm

            y_pred = regressor.predict(temp_log_s)

            predictions.append(dict({
                "y_preds": (np.concatenate(temp_log_s, axis=0), np.concatenate(y_pred, axis=0)),
                "regressor": regressor,
                "point": (i, point)
            }))

        return predictions

    def get_local_slopes(self):
        """
        Method for getting the calculated local slopes

        :return:
                local_slopes: [list] list of local slopes
        """
        local_slopes = []
        for local_reg in self.local_regression:
            local_slopes.append(np.asscalar(local_reg["regressor"].coef_))

        return local_slopes

    def visualize_local_slopes(self, fig=None):
        """
        Method for visualizing local slopes

        :param fig: [matplotlib figure]
        :return:
        """
        import matplotlib.pyplot as plt
        if fig == None:
            fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(40, 40))

        axes.plot(self.log_s, self.log_F)

        for local_reg in self.local_regression:
            axes.plot(local_reg["y_preds"][0], local_reg["y_preds"][1])

        axes.set(title='Log-log graph', xlabel='log(n)', ylabel='log(F)')  # , xlim=[-15, 15], ylim=[-15, 15]
        axes.legend(loc='upper left')
        axes.grid()


class OFSCA:
    def __init__(self, x_vector=None, y_vector=None, order=None, make_integration=True, start_scale=1, end_scale=2,
                 n_angles=None, refresh=666):
        """
        Class for performing OFSCA (Oriented Fluctuating Scaling Component Analysis)
        Main article describing methodology:

        :param x_vector: [1-D numpy array] X axis of 2-D trajectory
        :param y_vector: [1-D numpy array] Y axis of 2-D trajectory
        :param order: [0, 2, 4] order of detrending. describes what polynomial degree trend is removed during the detrending procedure
        :param make_integration: [True/False] pass True if integration is needed
        :param start_scale: [int/float] start values of scale for calculating local slopes to obtain orientations of the main components
        :param end_scale: [int/float] end values of scale for calculating local slopes to obtain orientations of the main components
        :param n_angles: [int] number of angles to calculate DDMA for, (180/n_angles) = angle_step
        """
        self.x_vector = x_vector
        self.y_vector = y_vector
        self.order = order
        self.make_integration = make_integration
        self.start_scale = start_scale
        self.end_scale = end_scale
        self.n_angles = n_angles
        self.refresh = refresh

    class ScalingComponent:
        def __init__(self):
            """
            Class for holding the main components time series and parameters
            """
            self.ts = None          # time series of decomposed components
            self.theta = None       # direction (angle) of the component
            self.theta_hat = None   # angle orthogonal to the main direction of the component
            self.log_F = None       # log(Fluctuantion function) of the component
            self.log_s = None       # log(scales)
            self.alpha = None       # Hurst exponent (alpha exponent) of the component

    def perform_OFSCA(self):
        """
        Method for performing the main OFSCA analysis.
        :return:
        """
        # get DDMA analysis
        self.theta_vector, self.log_s_matrix, self.log_F_matrix = self.get_ddma(x_vector=self.x_vector,
                                                                                y_vector=self.y_vector,
                                                                                order=self.order,
                                                                                make_intergration=self.make_integration,
                                                                                n_angles=self.n_angles,
                                                                                refresh=self.refresh)
        # get local slopes surface
        self.local_slopes = self.get_local_slopes_matrix()

        # get angle dependence of the slope in the range [self.start_scale, self.end_scale]
        self.slope_theta = self.get_angle_dependence_of_slope_in_range(start_scale=self.start_scale,
                                                                       end_scale=self.end_scale)

        # create main objects to hold the analysis result of the main components
        self.comp1 = self.ScalingComponent()
        self.comp2 = self.ScalingComponent()

        # get angles orthogonal to original orientation angles of components
        self.comp1.theta_hat, self.comp2.theta_hat = self.__calculate_independent_components_orthogonal_orientations(aplha_theta_vector=self.slope_theta)

        # get angles of orientations of independent components
        self.comp1.theta, self.comp2.theta = self.__calculate_independent_components_orientations(theta1_hat=self.comp1.theta_hat,
                                                                                                  theta2_hat=self.comp2.theta_hat)

        # get independent components
        self.comp1.ts, self.comp2.ts = self.get_independent_component_vectors(x_vector=self.x_vector, theta1=self.comp1.theta,
                                                                              y_vector=self.y_vector, theta2=self.comp2.theta)

        # get DMA analysis of independent components
        self.comp1.log_s, self.comp1.log_F = self.get_cdma(x_vector=self.comp1.ts, order=self.order, make_intergration=self.make_integration)
        self.comp2.log_s, self.comp2.log_F = self.get_cdma(x_vector=self.comp2.ts, order=self.order, make_intergration=self.make_integration)

        # get Hurst exponent of independent components in log_s[start_scale:end_scale] range
        str_idx, _ = self.get_the_closest_value(list=self.comp1.log_s, value=self.start_scale)
        end_idx, _ = self.get_the_closest_value(list=self.comp1.log_s, value=self.end_scale)
        self.comp1.alpha, _ = self.__calculate_scaling_exponent_in_range(self.comp1.log_s, self.comp1.log_F, start_scale=str_idx, end_scale=end_idx)
        self.comp2.alpha, _ = self.__calculate_scaling_exponent_in_range(self.comp2.log_s, self.comp2.log_F, start_scale=str_idx, end_scale=end_idx)


    def get_ddma(self, x_vector=None, y_vector=None, order=None, make_intergration=False, n_angles=64, refresh=666):
        """
        Function for getting DDMA (Directional Detrended Moving Average) analysis result.
        The analysis is performed for 2-D trajectory.

        :param x_vector: [1-D numpy array] X axis of 2-D trajectory.
        :param y_vector: [1-D numpy array] Y axis of 2-D trajectory.
        :param order: [0, 2, 4] order of detrending. describes what polynomial degree trend is removed during the detrending procedure
        :param make_intergration: [True/False] pass True if integration is needed
        :param n_angles: [int] number of angles to calculate DDMA for, (180/n_angles) = angle_step
        :return:
                angle_vector: [1-D numpy array] angles of axis projection. len = [1, n_angles, (180/n_angles)]
                log_s_matrix: [2-D numpy matrix] log scale for each angle direction
                log_F_matrix: [2-D numpy matrix] log fluctuation function for each angle direction
        """
        angle_vector, log_F_matrix, log_s_vector = get_directedDMA(x_vector,
                                                                   y_vector,
                                                                   n_angles=n_angles,
                                                                   integrate=1 if make_intergration else 0,
                                                                   asym_intervals=0,
                                                                   order=order,
                                                                   refresh=refresh)

        mtr_size = log_F_matrix.shape

        # create a matrix of scales
        log_s_matrix = np.zeros(mtr_size)
        for i, angle in enumerate(angle_vector):
            log_s_matrix[i, :] = np.copy(log_s_vector)

        # get rid of negative values by adding min value for every matrix element. don't affect the slope
        log_F_matrix = log_F_matrix + np.abs(log_F_matrix.min()) if log_F_matrix.min() < 0 else log_F_matrix

        return angle_vector, log_s_matrix, log_F_matrix

    def get_cdma(self, x_vector=None, order=None, make_intergration=True, asym_intervals=0, refresh=666):
        """
        Function for getting cDMA (centered Detrended Moving Average)
        Algorithm is described in

        :param x_vector: [1-D numpy array] time series for calculating cDMA
        :param order: [0, 2, 4] order of detrending. describes what polynomial degree trend is removed during the detrending procedure
        :param make_intergration: [True/False] pass True if integration is needed
        :param asym_intervals: [True/False]
        :param refresh: [int]
        :return:
                log_s: [1-D numpy array] vector of log scale
                log_F: [1-D numpy array] vector of log fluctuation function
        """
        log_s, log_F = get_cDMA(x_vector,
                                integrate=1 if make_intergration else 0,
                                asym_intervals=asym_intervals,
                                order=order,
                                refresh=refresh)

        return log_s, log_F

    def get_local_slopes(self, log_s, log_F, points_for_estimation=10):
        """
        Method for getting vector of local slopes

        :param log_s: [1-D numpy array] vector of log scale
        :param log_F: [1-D numpy array] vector of log fluctuation function
        :param points_for_estimation: [int] points for estimating the local slopes, default: 10
        :return:
        """
        lr = LocalRegression(log_s, log_F, points_for_estimation)
        local_slopes = np.copy(lr.get_local_slopes())
        return local_slopes

    def get_local_slopes_matrix(self, points_for_estimation=20):
        """
        Method for getting local slopes matrix

        :param points_for_estimation: [int] points for estimating the local slopes, default: 20
        :return:
                local_slopes: [2-D numpy array] matrix of local slopes, rows correspond to each rotation angle
        """
        local_slopes = np.zeros(self.log_F_matrix.shape)
        for index in range(self.log_F_matrix.shape[0]):
            local_slopes[index, :] = self.get_local_slopes(self.log_s_matrix[index, :],
                                                           self.log_F_matrix[index, :],
                                                           points_for_estimation)

        return local_slopes

    def __rotate_vector(self, vector, angle):
        """
        Method that performs vector rotation on a certain angle.

        :param vector: [1-D numpy array] vector to rotate
        :param angle: [int] [0-360] angle to rotate
        :return:
                [1-D numpy array] rotated vector
        """

        # get the rotation matrix
        def __get_rot_matrix(theta):
            c, s = np.cos(theta), np.sin(theta)
            R = np.array(((c, -s), (s, c)))
            return R

        # make the multiplication
        c = np.asarray([dist * (np.matmul(__get_rot_matrix(angle), np.array([2 * np.pi, 0]))) for dist in vector])
        return c.T

    def __create_polar_plot(self, ax=None, levels=None, r_vector=None, z_matrix=None, title="", norm=None, **kwargs):
        """
        Method for creating a polar plot of fluctuation function surface or local slopes surface.

        :param ax: [axes obj] matplotlib axes object to plot the polar plot at. Must have [projection="polar"] property
        :param levels: [int] how many iso projection levels to devide the surface to
        :param r_vector: []
        :param z_matrix: []
        :param title: [str] title of the polar plot
        :param norm: [list] [min_value, max_value] min and max values to normalize colors
        :param kwargs: kwargs arguments for some properties of polar plot
                        title_fontsize: [int] font size of the title. default: 18
                        title_pad: [int] pad of the title, default: 30
                        cbar_tick_labelsize: [int] colorbar label size, default: 12
                        contour_label_fontsize: [int] font size of the contour labels, default: 12
                        contour_color: [] contour color, default: 'black'
                        angles: [list] angles to highlight on polar plot, default: [theta * 45 for theta in range(360 // 45)]
                        angle_labels: [list] list of angle labels, default: [r'0', r'$\frac{\pi}{4}$', r'$\frac{\pi}{2}$', r'$\frac{3\pi}{4}$',
                                                                   r'$\pi$', r'$\frac{5\pi}{4}$', r'$\frac{3\pi}{2}$',
                                                                   r'$\frac{7\pi}{4}$'])
                        angles_labels_size: [int] font size of the angle labels, default: 12
                        angles_pad = [int] pad of the angle labels, default: 10
                        cmap: [matplotlib ColorMap obj] colormap of the surface, default: cm.coolwarm
                        colorbar_pad: [int/float] colorbar pad, default: 0.1
                        colorbar_shrink: [int/float] colorbar shrink, default: 0.6
        :return:
        """
        title_fontsize = kwargs.get('title_fontsize', 18)
        title_pad = kwargs.get('title_pad', 30)
        cbar_tick_labelsize = kwargs.get('cbar_tick_labelsize', 12)
        contour_label_fontsize = kwargs.get('contour_label_fontsize', 12)
        contour_color = kwargs.get('contour_color', 'black')
        angles = kwargs.get('angles', [theta * 45 for theta in range(360 // 45)])
        angle_labels = kwargs.get('angle_labels', [r'0', r'$\frac{\pi}{4}$', r'$\frac{\pi}{2}$', r'$\frac{3\pi}{4}$',
                                                   r'$\pi$', r'$\frac{5\pi}{4}$', r'$\frac{3\pi}{2}$',
                                                   r'$\frac{7\pi}{4}$'])
        angles_labels_size = kwargs.get('angles_labels_size', 12)
        angles_pad = kwargs.get('angles_pad', 10)
        cmap = kwargs.get('cmap', cm.coolwarm)
        colorbar_pad = kwargs.get('colorbar_pad', 0.1)
        colorbar_shrink = kwargs.get('colorbar_shrink', 0.6)

        # create meshgrid and polar plot
        rad = r_vector
        azm = np.linspace(0, 2 * np.pi, z_matrix.shape[0])
        r, th = np.meshgrid(rad, azm)
        plot = [th, r, z_matrix]

        # normalize colors
        if norm is not None:
            norm_colors = mpl.colors.Normalize(*norm)
        else:
            norm_colors = mpl.colors.Normalize(*[np.min(z_matrix), np.max(z_matrix)])

        # create polar plot
        surf = ax.contourf(*plot, levels, cmap=cmap, norm=norm_colors)

        # plot countours
        contours = ax.contour(*plot, levels, colors=contour_color)
        ax.clabel(contours, inline=1, fontsize=contour_label_fontsize)

        # create colorbar
        cbar = plt.colorbar(surf, ax=ax, pad=colorbar_pad, shrink=colorbar_shrink, aspect=7, cmap=cmap,
                            norm=norm_colors,
                            orientation="vertical")
        cbar.ax.tick_params(labelsize=cbar_tick_labelsize)

        ax.set_yticklabels([])
        ax.grid(b=False)
        ax.set_title(title, pad=title_pad, fontsize=title_fontsize)

        # set angle labels
        if len(angles) != len(angle_labels):
            raise ValueError("len(angles) != len(angle_labels). Please, define equal lists size.")
        ax.set_thetagrids(angles, labels=angle_labels)
        ax.tick_params(pad=angles_pad, labelsize=angles_labels_size)

        ax.set_rmax(np.max(r_vector))
        ax.set_rmin(np.min(r_vector))

        return

    def __create_z_matrix(self, angles=None, half_circle_mtrx=None):
        """
        Method for creating the z_matrix for polar 360 degrees plot

        :param angles: [1-D numpy array] vector of angles
        :param half_circle_mtrx: [2-D numpy array] half cirle matrix
        :return:
                z_surf: [2-D numpy array] 360 degrees z surface
        """
        # create a matrix corresponding to each direction among 360 degrees
        mtr_size = (angles.shape[0] * 2 + 1, half_circle_mtrx.shape[1])
        z_surf = np.zeros(mtr_size)
        for j, angle in enumerate(angles):
            z_surf[j, :] = np.copy(half_circle_mtrx[j, :])
            z_surf[angles.shape[0] + 1 + j, :] = np.copy(half_circle_mtrx[j, :])
        z_surf[angles.shape[0], :] = np.copy(z_surf[0, :])

        return z_surf

    def highlight_angles_on_polar_plot(self, axes=None, angles_to_highlight=None, colors=None):
        """
        Method that highlights certain angles on polar plot.

        :param axes: [matplolib axes obj] axes to visualize on
        :param angles_to_highlight: [list] list of angles to highlight
        :param colors: [list] list of colors of angles that need to be highlighted.

        len(colors) == len(angles_to_highlight), otherwise ValueError is thrown
        :return: None
        """
        if len(colors) != len(angles_to_highlight):
            raise ValueError("len(colors) != len(direction_angles). Please, define equal lists size.")

        for i, rad in enumerate(angles_to_highlight):
            axes.plot([rad, rad], [np.min(self.log_s_matrix[0, :]), np.max(self.log_s_matrix[0, :])], linestyle='--',
                      color=colors[i], linewidth=3)
            rad_op = rad + np.pi if rad < 0 else rad - np.pi
            axes.plot([rad_op, rad_op], [np.min(self.log_s_matrix[0, :]), np.max(self.log_s_matrix[0, :])],
                      linestyle='--',
                      color=colors[i], linewidth=3)

    def visualize_local_slopes_surface(self, local_slopes_mtrx=None, log_s_matrix=None, theta_v=None,
                                       ax=None, levels=None, title="", normalize=[], **kwargs):
        """
        Method for visualizing local slope surface
        Method creates a matrix corresponding to each direction among 360 degrees
        local_slopes_mtrx is only calculated for 180 degrees, as other directions (180-360) can be obtained by mirroring the original local_slopes_mtrx

        :param local_slopes_mtrx: [2-D numpy array] slopes matrix, each row corresponds to each rotation angle
        :param log_s_matrix: [2-D numpy array] log scale matrix, each row corresponds to each rotation angle
        :param theta_v: [1-D numpy array] vector of rotation angles
        :param ax: [matplolib axes obj] axes to visualize on
        :param levels: [int] how many iso projection levels to devide the surface to
        :param title: [str] plot title
        :param normalize: [list] [min_value, max_value] min and max values to normalize colors
        :param kwargs: see self.__create_polar_plot() kwargs argument description
        :return: None
        """
        z_local_slopes = self.__create_z_matrix(half_circle_mtrx=local_slopes_mtrx, angles=theta_v)

        self.__create_polar_plot(ax=ax,
                                 levels=levels,
                                 r_vector=log_s_matrix[0, :],
                                 z_matrix=z_local_slopes,
                                 title=title,
                                 norm=normalize if normalize is not [] else None,
                                 **kwargs)

        return

    def visualize_fluctuation_function_surface(self, log_F_mtrx=None, log_s_matrix=None, theta_v=None,
                                               ax=None, levels=None, title="", normalize=[], **kwargs):
        """
        Method for visualizing fluctuation function surface
        Method creates a matrix corresponding to each direction among 360 degrees
        log_F_matrix is only calculated for 180 degrees, as other directions (180-360) can be obtained by mirroring the original log_F_matrix

        :param log_F_mtrx: [2-D numpy array] log fluctuation function matrix, each row corresponds to each rotation angle
        :param log_s_matrix: [2-D numpy array] log scale matrix, each row corresponds to each rotation angle
        :param theta_v: [1-D numpy array] vector of rotation angles
        :param ax: [matplolib axes obj] axes to visualize on
        :param levels: [int] how many iso projection levels to devide the surface to
        :param title: [str] plot title
        :param normalize: [list] [min_value, max_value] min and max values to normalize colors
        :param kwargs: see self.__create_polar_plot() kwargs argument description
        :return: None
        """
        # create a matrix corresponding to each direction among 360 degrees
        # log_F_matrix is only calculated for 180 degrees, as other directions (180-360)
        # can be obtained by mirroring the original log_F_matrix
        z_log_F = self.__create_z_matrix(half_circle_mtrx=log_F_mtrx, angles=theta_v)

        self.__create_polar_plot(ax=ax,
                                 levels=levels,
                                 r_vector=log_s_matrix[0, :],
                                 z_matrix=z_log_F,
                                 title=title,
                                 norm=normalize if normalize is not [] else None,
                                 **kwargs)

        return

    def get_the_closest_value(self, list=None, value=None):
        """
        Method for getting the closest possible value in the list

        :param list: [list/ numpy array] list to search a value in
        :param value: [int, float] value to search
        :return:
                idx: [int] index of the closest value
                lst[idx]: closest value
        """
        lst = np.asarray(list)
        idx = (np.abs(list - value)).argmin()
        return idx, lst[idx]

    def get_fluctuation_function_curve_at_angle(self, angle=None):
        """
        Method for extracting the log F(log n) - fluctuation function at given angle
        :param angle: [float] angle at which to extract fluctuation curve
        :return:
                log_s: [1-D numpy array] log scale vector at given angle
                log_F: [1-D numpy array] log fluctuation function vector at given angle
        """

        idx, val = self.get_the_closest_value(self.theta_vector, angle)
        print(f"fluct_func - idx: {idx}, val: {val}")
        return self.log_s_matrix[idx, :], self.log_F_matrix[idx, :]

    def get_local_slope_curve_at_angle(self, angle=None):
        """
        Method for extracting the local_slope(log n) - local slope function at given angle
        :param angle: [float] angle at which to extract local slope curve
        :return:
                log_s: [1-D numpy array] log scale vector at given angle
                log_F: [1-D numpy array] local slope function vector at given angle
        """

        idx, val = self.get_the_closest_value(self.theta_vector, angle)
        print(f"slope - idx: {idx}, val: {val}")
        return self.log_s_matrix[idx, :], self.local_slopes[idx, :]

    def __calculate_scaling_exponent_in_range(self, log_s, log_F, start_scale=None, end_scale=None):
        """
        Method for calculating scaling exponent in certain scale range

        :param log_s: [1-D numpy array] log scale vector
        :param log_F: [1-D numpy array] log fluctuation function vector
        :param start_scale: [int/float] start values of scale
        :param end_scale: [int/float] end values of scale
        :return:
                reg.coef_[0]: [int/float] slope of the fitted linear regression curve, that corresponds to alpha exponent value
                reg.intercept_: [int/float] intercept of the fitted linear regression curve
        """

        if start_scale is None:
            start_scale = 0
        if end_scale is None:
            end_scale = len(log_s)

        x = np.expand_dims(np.asarray(log_s[start_scale:end_scale]), axis=1)
        y = np.asarray(log_F[start_scale:end_scale])

        reg = LinearRegression().fit(x, y)
        reg.score(x, y)

        return reg.coef_[0], reg.intercept_

    def get_angle_dependence_of_slope_in_range(self, start_scale, end_scale):
        """
        Method for getting the angle dependence of local slope in certain scale range

        :param start_scale: [int/float] start values of scale
        :param end_scale: [int/float] end values of scale
        :return:
                slope_vector: [1-D numpy array] vector with local slopes in each direction
        """
        slope_vector = []
        end_n_index, _ = self.get_the_closest_value(self.log_s_matrix[0, :], end_scale)
        start_n_index, _ = self.get_the_closest_value(self.log_s_matrix[0, :], start_scale)

        for i, angle in enumerate(self.theta_vector):
            (alpha, _) = self.__calculate_scaling_exponent_in_range(self.log_s_matrix[i, :],
                                                                    self.log_F_matrix[i, :],
                                                                    start_scale=start_n_index,
                                                                    end_scale=end_n_index)
            slope_vector.append(alpha)

        return np.array(slope_vector)

    def get_independent_component_vectors(self, x_vector, y_vector, theta1, theta2):
        """
        Method for extracting independent components from 2-D trajectory

        :param x_vector: [1-D numpy array] X axis of 2-D trajectory
        :param y_vector: [1-D numpy array] Y axis of 2-D trajectory
        :param theta1: [float] angle of the component with higher Hurst exponent value
        :param theta2: [float] angle of the component with lower Hurst exponent value
        :return:
                comp1: [1-D numpy array] component with maximum slope value
                comp2: [1-D numpy array] component with minimum slope value
        """
        # max component
        comp1 = x_vector * (np.sin(theta2) / np.sin(theta2 - theta1)) + y_vector * (
                np.cos(theta2) / np.sin(theta1 - theta2))
        # min component
        comp2 = x_vector * (np.sin(theta1) / np.sin(theta1 - theta2)) + y_vector * (
                np.cos(theta1) / np.sin(theta2 - theta1))

        return comp1, comp2

    def __calculate_independent_components_orientations(self, theta1_hat, theta2_hat):
        """
        Method for calculating independent components orientations

        :param theta1_hat: [float] angle, orthogonal to angle of component with higher Hurst exponent value
        :param theta2_hat: [float] angle, orthogonal to angle of component with lower Hurst exponent value
        :return:
                theta1: [float] direction of the component with maximum slope value
                theta2: [float] direction of the component with minimum slope value
        """
        theta1 = theta1_hat - np.pi / 2 if theta1_hat >= np.pi / 2 else theta1_hat + np.pi / 2  # max angles
        theta2 = theta2_hat - np.pi / 2 if theta2_hat >= np.pi / 2 else theta2_hat + np.pi / 2  # min angle

        return theta1, theta2

    def __calculate_independent_components_orthogonal_orientations(self, aplha_theta_vector):
        """
        Method for finding min. max value on the slope(theta) graph, thus obtaining angles which
        corresponds to angles orthogonal to orientations of the main components

        :param aplha_theta_vector: slope (Hurst exponent) - angle dependency
        :return:
                theta1_hat: [float] direction of the component with min slope(theta) value, which corresponds to max component
                theta2_hat: [float] direction of the component with max slope(theta) value, which corresponds to min component
        """
        # define min and max slope
        min_slope_value, min_slope_idx = np.min(aplha_theta_vector), np.argmin(aplha_theta_vector)
        max_slope_value, max_slope_idx = np.max(aplha_theta_vector), np.argmax(aplha_theta_vector)

        # extract theta1 hat and theta2 hat values
        theta1_hat = self.theta_vector[min_slope_idx]
        theta2_hat = self.theta_vector[max_slope_idx]

        return theta1_hat, theta2_hat

    def get_ofsca_fig(self, rcParams=None, alpha_graphs_ylim=None):
        """
        Method for getting the typical OFSCA figure

        :param rcParams: [dict] matplotlib rcParams dictionary
        :param alpha_graphs_ylim: [list/tuple] [min_value, max_value] list of min/max values to normalize alpha exponent plots
        default: min and max values of the corresponding graph
        :return:
                fig: [matplotlib figure obj] matplotlib figure with the obtained result
        """
        if rcParams is not None:
            plt.rcParams.update(rcParams)

        fig = plt.figure()
        fig.set_size_inches(20, 12, forward=True)
        from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
        gs = GridSpec(nrows=2, ncols=3, hspace=0.4, wspace=0.3)

        # plot [0,0] - 2D trajectory view
        ax1 = fig.add_subplot(gs[0, 0])
        ax1.plot(self.x_vector, self.y_vector)
        ax1.grid(b=True)
        ax1.set_aspect("equal", "datalim")
        ax1.set_xlabel(f"$x[i]$")
        ax1.set_ylabel(f"$y[i]$")
        ax1.set_title('(a)', fontsize=18)

        # plot [0,1] - Fluctuation function surface
        ax2 = fig.add_subplot(gs[0, 1], projection="polar")
        self.visualize_fluctuation_function_surface(log_F_mtrx=self.log_F_matrix,
                                                    log_s_matrix=self.log_s_matrix,
                                                    theta_v=self.theta_vector,
                                                    ax=ax2,
                                                    title="(b)",
                                                    levels=6,
                                                    angles_labels_size=16,
                                                    angles_pad=5)

        self.highlight_angles_on_polar_plot(axes=ax2,
                                            angles_to_highlight=[self.comp1.theta, self.comp2.theta],
                                            colors=["r", "b"])

        # plot [0,2] - Local slope surface
        ax3 = fig.add_subplot(gs[0, 2], projection="polar")
        self.visualize_local_slopes_surface(local_slopes_mtrx=self.local_slopes[:, 5:-5],
                                            log_s_matrix=self.log_s_matrix[:, 5:-5],
                                            theta_v=self.theta_vector,
                                            ax=ax3,
                                            levels=6,
                                            title="(c)",
                                            angles_labels_size=16,
                                            angles_pad=5)

        self.highlight_angles_on_polar_plot(axes=ax3,
                                            angles_to_highlight=[self.comp1.theta, self.comp2.theta],
                                            colors=["r", "b"])

        # plot [1,0] - angle dependence of local slopes slopes(theta) in range[self.start_scale, self.end_scale]
        ax4 = fig.add_subplot(gs[1, 0])
        ax4.plot(self.theta_vector, self.slope_theta, linewidth=2)

        theta1_hat_idx = np.where(self.theta_vector == self.comp1.theta_hat)[0][0]
        theta2_hat_idx = np.where(self.theta_vector == self.comp2.theta_hat)[0][0]
        min_slope_value = np.min(self.slope_theta)
        max_slope_value = np.max(self.slope_theta)

        ax4.plot(self.theta_vector[theta1_hat_idx], min_slope_value, "ro",
                 label=fr"$\^\theta_1$ = {np.degrees(self.theta_vector[theta1_hat_idx]):0.0f}$^\circ, \alpha({np.degrees(self.theta_vector[theta1_hat_idx]):0.0f}^\circ) = {min_slope_value:0.2f}$")

        ax4.plot(self.theta_vector[theta2_hat_idx], max_slope_value, "bo",
                 label=fr"$\^\theta_2$ = {np.degrees(self.theta_vector[theta2_hat_idx]):0.0f}$^\circ, \alpha({np.degrees(self.theta_vector[theta2_hat_idx]):0.0f}^\circ) = {max_slope_value:0.2f}$")
        ax4.axvline(x=self.theta_vector[theta1_hat_idx], color='r', linestyle='--', linewidth=2)
        ax4.axvline(x=self.theta_vector[theta2_hat_idx], color='b', linestyle='--', linewidth=2)
        ax4.legend(loc="lower right", fontsize=12)
        ax4.grid(b=True)
        ax4.set_title('(d)', fontsize=18)
        ax4.set_xlabel(r"$\theta, rad$")
        ax4.set_ylabel(r"slope")
        if alpha_graphs_ylim is not None:
            ax4.set_ylim(*alpha_graphs_ylim)
        else:
            ax4.set_ylim(np.min(self.slope_theta) - 0.1, np.max(self.slope_theta) + 0.1)

        str_idx, _ = self.get_the_closest_value(list=self.comp2.log_s, value=self.start_scale)
        end_idx, _ = self.get_the_closest_value(list=self.comp2.log_s, value=self.end_scale)

        import matplotlib
        cmap = matplotlib.cm.get_cmap('coolwarm')

        ax5 = fig.add_subplot(gs[1, 1])
        ax5.plot(self.comp1.ts, self.comp2.ts)
        ax5.grid(b=True)
        ax5.set_aspect("equal", "datalim")
        ax5.set_xlabel(r"$\^\epsilon_1[i]$")
        ax5.set_ylabel(r"$\^\epsilon_2[i]$")
        ax5.set_title('(e)', fontsize=18)

        ax6 = fig.add_subplot(gs[1, 2])
        ax6.plot(self.comp1.log_s, self.comp1.log_F, color=cmap(1.0), linestyle='--', linewidth=1, marker='o',
                 label=fr"$\epsilon_1, \theta_1 = {np.degrees(self.comp1.theta):0.0f}^\circ, \alpha(\epsilon_1) = {self.comp1.alpha:0.2f}$")
        ax6.plot(self.comp2.log_s, self.comp2.log_F, color=cmap(0.0), linestyle='--', linewidth=1, marker='o',
                 label=fr"$\epsilon_2, \theta_2 = {np.degrees(self.comp2.theta):0.0f}^\circ, \alpha(\epsilon_2) = {self.comp2.alpha:0.2f}$")

        ax6.vlines(self.comp1.log_s[str_idx], np.min(self.comp1.log_F), np.max(self.comp1.log_F), color='gray', linewidth=2,
                   linestyle='--')
        ax6.vlines(self.comp1.log_s[end_idx], np.min(self.comp1.log_F), np.max(self.comp1.log_F), color='gray', linewidth=2,
                   linestyle='--')
        ax6.set_title("(f)", fontsize=18)
        ax6.grid(b=True)
        ax6.set_xlabel(r"$log_{10} s$")
        ax6.set_ylabel(r"$log_{10} F^(\theta)$")
        ax6.set_frame_on(True)
        ax6.legend(fontsize=12)

        r, p_value = scipy.stats.pearsonr(self.comp1.ts, self.comp2.ts)
        print(f"Pearson correlation between components: R, P_value: {r}, {p_value}")

        return fig
