from setuptools import Extension, setup, find_packages
import numpy


def main():
    ddma_module = Extension("ofsca.ddma",
                       sources=[
                           'ofsca/ddma/direcDMA0.c',
                           'ofsca/ddma/direcDMA2.c',
                           'ofsca/ddma/direcDMA4.c',
                           'ofsca/ddma/memory.c',
                           'ofsca/ddma/ddma.c'
                       ],
                       include_dirs=[numpy.get_include(), "ofsca/ddma/"],
                       library_dirs=['ofsca/ddma/'])

    dma_module = Extension("ofsca.cdma",
                            sources=[
                                'ofsca/cdma/cDMA0.c',
                                'ofsca/cdma/cDMA2.c',
                                'ofsca/cdma/cDMA4.c',
                                'ofsca/cdma/memory.c',
                                'ofsca/cdma/cdma.c'
                            ],
                            include_dirs=[numpy.get_include(), "ofsca/cdma/"],
                            library_dirs=['ofsca/cdma/'])

    setup(name='ofsca',
          version='1.1',
          description='Python wrapper for directed DMA',
          author="Ken Kiyono, Ivan Seleznov",
          author_email="ivan.seleznov1@gmail.com",
          ext_modules=[ddma_module, dma_module],
          packages=find_packages(),
          include_package_data=True,
          setup_requires=['numpy>=1.14',
                          'matplotlib',
                          'sklearn',
                          'scipy']
        )


if __name__ == "__main__":
    main()
